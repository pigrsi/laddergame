package com.pigrsi.dan.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.pigrsi.dan.LadderGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Super Ladder Puzzle Game";
		config.x = 400;
		config.y = 0;
		config.width = 1000;
		config.height = 600;
		new LwjglApplication(new LadderGame(), config);
	}
}
