package com.pigrsi.dan;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

public class Util {

    public static TiledMapTileLayer getLayerFromMap(TiledMap map, int layerId) {
        TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(layerId);
        if (layer == null) { Gdx.app.error("Util", "Null layer in spawnEntitiesOnLayer method."); return null; }
        return layer;
    }

    public static TiledMapTileLayer getLayerFromMap(TiledMap map, String layerName) {
        TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(layerName);
        if (layer == null) { Gdx.app.error("Util", "Null layer in spawnEntitiesOnLayer method."); return null; }
        return layer;
    }

}
