package com.pigrsi.dan.gameworld;

public interface WorldEventListener {
    void onWorldEvent(WorldEvent event);
}
