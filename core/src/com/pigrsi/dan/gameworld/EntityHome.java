package com.pigrsi.dan.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.pigrsi.dan.entities.block.*;
import com.pigrsi.dan.entities.ladder.CrapLadder;
import com.pigrsi.dan.entities.ladder.Ladder;
import com.pigrsi.dan.entities.ladder.PowerLadder;
import com.pigrsi.dan.entities.misc.Player;
import com.pigrsi.dan.entities.misc.Rail;
import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.entities.types.RotaryEntity;

import java.lang.reflect.Constructor;
import java.util.*;

public class EntityHome {
    public List<Entity> movableEntities;
    public List<Entity> nonMovableEntities;

    public List<Entity> stateTrackingEntities;

    public DrawingOrder drawingOrder;
    public List<RotaryEntity> justRotated;
    public Player player;

    public EntityHome() {
        nonMovableEntities = new ArrayList<>();
        movableEntities = new ArrayList<>();
        stateTrackingEntities = new ArrayList<>();
        drawingOrder = new DrawingOrder(4);
        justRotated = new ArrayList<>();
    }

    public void spawnEntitiesFromLayer(TiledMapTileLayer layer, Grid grid) {
        if (layer == null) return;
        MapEntitySpawner mapEntitySpawner = new MapEntitySpawner();
        mapEntitySpawner.spawnEntitiesOnLayer(this, grid, layer);
    }

    public void orderEntities() {
        Collections.sort(movableEntities, new Comparator<Entity>() {
            @Override
            public int compare(Entity e1, Entity e2) {
                if (e1 instanceof Ladder) return 1;
                if (e2 instanceof Ladder) return -1;
                if (e1.getClass() == e2.getClass()) return 0;
                return 0;
            }
        });
    }

    public List<Entity> getAllEntities() {
        Set<Entity> entities = new HashSet<>();
        entities.addAll(nonMovableEntities);
        entities.addAll(movableEntities);
        return new ArrayList<>(entities);
    }

    public void dispose() {
        for (Entity entity: getAllEntities()) entity.dispose();
    }

    /** SPAWN METHODS **/
    public Entity spawnEntity(Grid grid, Class type, int row, int column, List<Entity>... listsToAddTo) {
        try {
            Class<?> cl = Class.forName(type.getName());
            Constructor<?> cons = cl.getConstructor(Grid.class);
            Entity entity = (Entity) cons.newInstance(grid);
            grid.add(entity, row, column);
            for (List<Entity> list: listsToAddTo)
                list.add(entity);
            return entity;
        } catch (Exception e) {
            Gdx.app.error("spawnEntity", "Error spawning entity", e);
        }
        return null;
    }

    public Ladder spawnLadder(Grid grid, Entity.Orientation orientation, int row, int column) {
        Ladder newLadder = (Ladder) spawnEntity(grid, Ladder.class, row, column, movableEntities, stateTrackingEntities, drawingOrder.getDrawingList(1));
        newLadder.setOrientation(orientation);
        return newLadder;
    }

    public CrapLadder spawnCrapLadder(Grid grid, Entity.Orientation orientation, int row, int column) {
        CrapLadder newLadder = (CrapLadder) spawnEntity(grid, CrapLadder.class, row, column, movableEntities, stateTrackingEntities, drawingOrder.getDrawingList(1));
        newLadder.setOrientation(orientation);
        return newLadder;
    }

    public PowerLadder spawnPowerLadder(Grid grid, Entity.Orientation orientation, int row, int column) {
        PowerLadder newLadder = (PowerLadder) spawnEntity(grid, PowerLadder.class, row, column, movableEntities, stateTrackingEntities, drawingOrder.getDrawingList(1));
        newLadder.setOrientation(orientation);
        return newLadder;
    }

    public Block spawnBlock(Grid grid, int row, int column) { return (Block) spawnEntity(grid, Block.class, row, column, nonMovableEntities, drawingOrder.getDrawingList(0)); }

    public ThruBlock spawnThruBlock(Grid grid, int row, int column) { return (ThruBlock) spawnEntity(grid, ThruBlock.class, row, column, nonMovableEntities, drawingOrder.getDrawingList(0)); }

    public PowerBlock spawnPowerBlock(Grid grid, int row, int column) {
        PowerBlock powerBlock = (PowerBlock) spawnEntity(grid, PowerBlock.class, row, column, nonMovableEntities, stateTrackingEntities, drawingOrder.getDrawingList(3));
        List<Rail> rails = new ArrayList<>(grid.getNumRows() - row);
        while (++row < grid.getNumRows()) {
            rails.add(spawnRail(grid, row, column));
        }
        powerBlock.setRails(rails);
        return powerBlock;
    }

    public Player spawnPlayer(Grid grid, int row, int column) {
        player = (Player) spawnEntity(grid, Player.class, row, column, movableEntities, stateTrackingEntities, drawingOrder.getDrawingList(2));
        return player;
    }

    public Rail spawnRail(Grid grid, int row, int column) {
        return (Rail) spawnEntity(grid, Rail.class, row, column, movableEntities, stateTrackingEntities, drawingOrder.getDrawingList(0));
    }

    public Spike spawnSpike(Grid grid, int row, int column) {
        return (Spike) spawnEntity(grid, Spike.class, row, column, nonMovableEntities, drawingOrder.getDrawingList(0));
    }

    public Balloon spawnBalloon(Grid grid, int row, int column, Balloon.BalloonColour colour) {
        Balloon balloon = (Balloon) spawnEntity(grid, Balloon.class, row, column, movableEntities, stateTrackingEntities, drawingOrder.getDrawingList(0));
        balloon.setColour(colour);
        return balloon;
    }

    public StickyBlock spawnStickyBlock(Grid grid, int row, int column) {
        return (StickyBlock) spawnEntity(grid, StickyBlock.class, row, column, movableEntities, stateTrackingEntities, drawingOrder.getDrawingList(1));
    }

    static class DrawingOrder {
        List<List<Entity>> lists;

        DrawingOrder(int size) {
            lists = new ArrayList<>(size);
            for (int i = 0; i < size; ++i) {
                lists.add(new ArrayList<Entity>());
            }
        }

        List<Entity> getDrawingList(int level) {
            return lists.get(level);
        }

        int numLists() {
            return lists.size();
        }
    }
}
