package com.pigrsi.dan.gameworld;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.pigrsi.dan.gameworld.Grid;
import com.pigrsi.dan.gameworld.World;

public class CameraMan {
    private OrthographicCamera camera;

    public CameraMan(Grid grid) {
        camera = new OrthographicCamera(2400, 1600);
        camera.position.set(camera.viewportWidth / 2f, camera.viewportHeight / 2f, 0);
        camera.update();
        centreOnGrid(grid);
    }

    public void setProjectionMatrix(SpriteBatch batch) {
        batch.setProjectionMatrix(camera.combined);
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public void centreOnMapLayer(TiledMapTileLayer layer) {
        centreOnPosition(World.CELL_SIZE * layer.getWidth() / 2f, World.CELL_SIZE * layer.getHeight() / 2f);
    }

    private void centreOnGrid(Grid grid) {
        centreOnPosition(World.CELL_SIZE * grid.getNumColumns() / 2f, World.CELL_SIZE * grid.getNumRows() / 2f);
    }

    private void centreOnPosition(float x, float y) {
        camera.position.set(x, y, 0);
        camera.update();
    }

    public void resize(int width, int height) {
        camera.viewportWidth = width;
        camera.viewportHeight = height;
    }
}