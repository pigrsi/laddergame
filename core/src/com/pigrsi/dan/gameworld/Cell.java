package com.pigrsi.dan.gameworld;

import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.entities.types.FallingEntity;

import java.util.*;

public class Cell {
    private Set<Entity> entities = new HashSet<Entity>();
    private int row, column;
    private boolean killsEntities = false;

    public Cell(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public void add(Entity entity) {
        entities.add(entity);
    }

    public void remove(Entity entity) {
        entities.remove(entity);
    }

    public int getRow() { return row; }

    public int getColumn() { return column; }

    public float getWorldX() { return column * World.CELL_SIZE; }

    public float getWorldY() { return row * World.CELL_SIZE; }

    public Iterator<Entity> getEntityIterator() {
        return entities.iterator();
    }

    public Set<Entity> getEntities() { return entities; }

//    public List<Entity> getPushables() {
//        List<Entity> pushables = new ArrayList<>();
//        Iterator<Entity> it = getEntityIterator();
//        while (it.hasNext()) {
//            Entity next = it.next();
//            if (next.pushable()) pushables.add(next);
//        }
//        return pushables;
//    }

    public List<FallingEntity> getFallingEntities() {
        List<FallingEntity> fallingEntities = new ArrayList<>();
        Iterator<Entity> it = getEntityIterator();
        while (it.hasNext()) {
            Entity next = it.next();
            if (next instanceof FallingEntity) fallingEntities.add((FallingEntity) next);
        }
        return fallingEntities;
    }

//    public Entity getClimbable() {
//        Iterator<Entity> it = getEntityIterator();
//        while (it.hasNext()) {
//            Entity next = it.next();
//            if (next.isClimbable()) return next;
//        }
//        return null;
//    }

//    public List<Entity> getClimbables() {
//        List<Entity> climbables = new ArrayList<>();
//        Iterator<Entity> it = getEntityIterator();
//        while (it.hasNext()) {
//            Entity next = it.next();
//            if (next.isClimbable()) climbables.add(next);
//        }
//        return climbables;
//    }

//    public Entity getStillClimbable() {
//        Iterator<Entity> it = getEntityIterator();
//        while (it.hasNext()) {
//            Entity next = it.next();
//            if (next.isClimbable() && next.isStill()) return next;
//        }
//        return null;
//    }

    public Entity getFirstEntityInCell(Class entityType) {
        List<Entity> entities = new ArrayList<>();
        Iterator<Entity> it = getEntityIterator();
        while (it.hasNext()) {
            Entity next = it.next();
            if (entityType.isAssignableFrom(next.getClass())) return next;
        }
        return null;
    }

    public List<Entity> getEntitiesInCell(Class entityType) {
        List<Entity> entities = new ArrayList<>();
        Iterator<Entity> it = getEntityIterator();
        while (it.hasNext()) {
            Entity next = it.next();
            if (entityType.isAssignableFrom(next.getClass())) entities.add(next);
        }
        return entities;
    }

    public int getNumEntities() { return entities.size(); }

//    public boolean containsType(Class entityType) {
//        Iterator<Entity> it = getEntityIterator();
//        while (it.hasNext())
//            if (entityType.isAssignableFrom(it.next().getClass())) return true;
//        return false;
//    }

//    public boolean containsBlockingEntity() {
//        Iterator<Entity> it = getEntityIterator();
//        while (it.hasNext())
//            if (it.next().blocking()) return true;
//        return false;
//    }

//    public boolean containsEntityThatCanBeStoodOn() {
//        Iterator<Entity> it = getEntityIterator();
//        while (it.hasNext())
//            if (it.next().canBeStoodOn()) {
//                return true;
//            }
//        return false;
//    }

//    public boolean containsPushableEntity() {
//        Iterator<Entity> it = getEntityIterator();
//        while (it.hasNext())
//            if (it.next().pushable()) return true;
//        return false;
//    }

//    public boolean containsClimbable() {
//        Iterator<Entity> it = getEntityIterator();
//        while (it.hasNext())
//            if (it.next().isClimbable()) return true;
//        return false;
//    }

//    public boolean containsStillPushableEntity() {
//        Iterator<Entity> it = getEntityIterator();
//        while (it.hasNext()) {
//            Entity next = it.next();
//            if (next.pushable() && next.isStill()) return true;
//        }
//        return false;
//    }

//    public boolean containsStillEntityOfType(Class entityType) {
//        Iterator<Entity> it = getEntityIterator();
//        while (it.hasNext()) {
//            Entity next = it.next();
//            if (entityType.isAssignableFrom(next.getClass()) && next.isStill()) return true;
//        }
//        return false;
//    }

//    public boolean containsStillClimbable() {
//        Iterator<Entity> it = getEntityIterator();
//        while (it.hasNext()) {
//            Entity next = it.next();
//            if (next.isClimbable() && next.isStill()) return true;
//        }
//        return false;
//    }

    public static boolean cellsAreTouching(Cell c1, Cell c2) {
        return  c1 == c2 ||
                ( Math.abs(c1.getRow() - c2.getRow()) == 1 && c1.getColumn() == c2.getColumn() ) ||
                ( Math.abs(c1.getColumn() - c2.getColumn()) == 1 && c1.getRow() == c2.getRow() );
    }

    public static boolean cellsAreAdjacent(Cell cell1, Cell cell2) {
        return (cell1.getRow() == cell2.getRow()) && (Math.abs(cell1.getColumn() - cell2.getColumn())) == 1;
    }

    public void becomeDeathCell() {
        killsEntities = true;
    }

    public boolean isDeathCell() {
        return killsEntities;
    }
}
