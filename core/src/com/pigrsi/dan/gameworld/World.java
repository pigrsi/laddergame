package com.pigrsi.dan.gameworld;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.utils.Disposable;
import com.pigrsi.dan.Util;
import com.pigrsi.dan.input.InputReceiver;
import com.pigrsi.dan.rendering.DebugRenderer;

public class World implements Disposable {
    public static final int CELL_SIZE = 64;

    private MapLoader mapLoader;
    public EntityManager entityManager;
    public Grid grid;

    private SpriteBatch batch;
    private DebugRenderer debugRenderer;
    private CameraMan cameraMan;

    public void create(InputReceiver inputReceiver) {
        // Logic
        mapLoader = new MapLoader();
        TiledMap map = mapLoader.loadMap();
        entityManager = new EntityManager();
        TiledMapTileLayer[] layers = getEntityLayers(map);
        grid = new Grid(layers[0].getHeight(), layers[0].getWidth());
        cameraMan = new CameraMan(grid);
        entityManager.init(grid, layers);
        inputReceiver.addListener(new GameInput(entityManager));

        // Rendering
        batch = new SpriteBatch();
        debugRenderer = new DebugRenderer(this);
    }

    private TiledMapTileLayer[] getEntityLayers(TiledMap map) {
        TiledMapTileLayer[] layers = new TiledMapTileLayer[5];
        int i = 0;
        TiledMapTileLayer next;
        do {
            next = Util.getLayerFromMap(map, "Entities" + i);
            layers[i] = next;
            ++i;
        } while (next != null);
        return layers;
    }

    public void render(float delta) {
        /**** Updating ****/
        entityManager.update(delta);

        /**** Rendering ****/
        cameraMan.setProjectionMatrix(batch);
        debugRenderer.backRender(cameraMan.getCamera());

        batch.begin();
        entityManager.render(batch);
        batch.end();

        debugRenderer.frontRender(cameraMan.getCamera());
    }

    public void resize(int width, int height) {
        cameraMan.resize(width, height);
    }

    public void dispose() {
        batch.dispose();
        entityManager.dispose();
        mapLoader.dispose();
    }
}
