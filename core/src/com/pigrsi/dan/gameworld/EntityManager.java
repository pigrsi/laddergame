package com.pigrsi.dan.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.pigrsi.dan.GridStateListener;
import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.entities.ladder.Ladder;
import com.pigrsi.dan.entities.misc.Player;
import com.pigrsi.dan.entities.types.Mergable;
import com.pigrsi.dan.entities.types.RotaryEntity;
import com.pigrsi.dan.states.State;
import com.pigrsi.dan.states.StateManager;

import java.util.List;

public class EntityManager extends EntityHome implements GridStateListener {

    private StateManager stateManager;
    private Entity moveInstigator;
    private boolean moveInProgress = true;

    void init(Grid grid, TiledMapTileLayer ...entityLayers) {
        for (TiledMapTileLayer layer: entityLayers) spawnEntitiesFromLayer(layer, grid);
        orderEntities();
        entitySetup();
        grid.addGridStateListener(this);
        stateManager = new StateManager();
    }

    private void entitySetup() {
        for (Entity entity: movableEntities) {
            if (entity instanceof Mergable) ((Mergable) entity).checkMerge();
        }
    }

    public void update(float delta) {
        if (moveInstigator != null && allEntitiesAreStill()) {
            afterMoveIsMade();
            moveInstigator = null;
        }

        if (!canMakeMove()) checkMoveHasEnded();

        // Should update bottom to top?
        for (Entity entity: movableEntities)
            entity.update(delta);

        for (Entity entity: movableEntities)
            entity.postUpdate(delta);
    }

    private void checkMoveHasEnded() {
        if (entitiesHaveFinishedMoving()) {
            boolean endMove = true;
            for (Entity entity: getAllEntities()) {
                if (entity.checkForAutomove()) {
                    endMove = false;
                    break;
                }
            }
            if (endMove) moveInProgress = false;
        }
    }

    public void render(SpriteBatch batch) {
        for (int i = 0; i < drawingOrder.numLists(); ++i) {
            List<Entity> drawList = drawingOrder.getDrawingList(i);
            for (Entity entity: drawList) entity.render(batch);
        }
    }

    private boolean entitiesHaveFinishedMoving() {
        if (allEntitiesAreStill()) {
            if (!justRotated.isEmpty()) {
                validateRotations();
            } else {
                return true;
            }
        }
        return false;
    }

    private void validateRotations() {
        boolean rotationAllowed = true;
        for (RotaryEntity entity: justRotated) {
            Cell destCell = entity.getCell();
            if (!entity.canOverlapOnCell(destCell)) rotationAllowed = false;
            else if (destCell.isDeathCell()) rotationAllowed = false;
        }

        if (!rotationAllowed) {
            for (RotaryEntity entity: justRotated) { entity.reverseLastRotation(); }
        } else {
            for (RotaryEntity entity: justRotated) { entity.onRotationSuccess(); }
            justRotated.clear();
            afterMoveIsMade();
        }
    }

    void passMoveToPlayer(Entity.Direction direction) {
        player.tryMove(direction);
    }

    private boolean allEntitiesAreStill() {
        for (Entity entity: movableEntities)
            if (!entity.isStill() && entity.isAlive()) return false;
        return true;
    }

    private void afterMoveIsMade() {
        for (Entity entity: movableEntities)
            entity.onEntityMove();
    }

    @Override
    public void onGridStateChange(Entity instigator) {
        moveInstigator = instigator;
    }

    void addState(boolean incrementStateId) {
        if (incrementStateId) stateManager.incrementStateId();
        stateManager.addState(new State(stateTrackingEntities, stateManager.getNextStateId()));
    }

    boolean undoFromPlayer() {
        if (canUndo()) forceUndo();
        return true;
    }

    void forceUndo() { stateManager.undo(); }

    boolean inputDown() { movePlayer(Entity.Direction.DOWN); return true; }
    boolean inputUp() { movePlayer(Entity.Direction.UP); return true; }
    boolean inputLeft() { movePlayer(Entity.Direction.LEFT); return true; }
    boolean inputRight() { movePlayer(Entity.Direction.RIGHT); return true; }

    void movePlayer(Entity.Direction direction) {
        if (!canMakeMove()) return;
        addState(false);
        passMoveToPlayer(direction);
        moveInProgress = true;
        stateManager.incrementStateId();
    }

    boolean inputGrab() {
        if (!canMakeMove()) {
            Gdx.app.log(getClass().getSimpleName(), "Busy, can't try grab right now.");
            return true;
        }

        if (player.isHoldingSomething()) {
            addState(true);
            player.dropEntity();
            player.setMoveMode(Player.MoveMode.PLAYER);
            moveInProgress = true;
            afterMoveIsMade();
            return true;
        }
        Cell cell = player.getCell();
        Entity overlappingLadder = cell.getFirstEntityInCell(Ladder.class);
        if (overlappingLadder != null && player.getPreviousFallStopper() == Entity.FallStopper.FLOOR) {
            if (player.canGrabEntity(overlappingLadder)) {
                addState(true);
                player.grabEntity(overlappingLadder);
            } else {
                Gdx.app.log(getClass().getSimpleName(), "Player not allowed to grab this entity right now.");
            }
        } else {
            Gdx.app.log(getClass().getSimpleName(), "Player cannot grab anything.");
        }
        return true;
    }

    boolean switchMoveMode() {
        if (!canMakeMove()) {
            Gdx.app.log(getClass().getSimpleName(), "Busy, can't change move mode right now.");
            return true;
        }
        addState(true);
        if (player.getMoveMode() == Player.MoveMode.PLAYER_GRABBED)
            player.setMoveMode(Player.MoveMode.PLAYER);
        else if (player.isHoldingSomething())
            player.setMoveMode(Player.MoveMode.PLAYER_GRABBED);
        return true;
    }

    boolean rotateLeft() {
        rotate(Entity.RotationDirection.ANTICLOCKWISE);
        return true;
    }

    boolean rotateRight() {
        rotate(Entity.RotationDirection.CLOCKWISE);
        return true;
    }

    boolean canMakeMove() {
        return !moveInProgress && player.isAlive();
    }

    boolean canUndo() {
        return !moveInProgress;
    }

    private void rotate(Entity.RotationDirection rotation) {
        if (!canMakeMove()) {
            Gdx.app.log(getClass().getSimpleName(), "Busy, can't rotate right now.");
            return;
        }
        if (player.getGrabbedEntity() == null) return;
        addState(true);
        for (Entity entity: player.getGrabbedEntity().getGroup()) {
            if (entity instanceof RotaryEntity) {
                RotaryEntity rotaryEntity = ((RotaryEntity) entity);
                rotaryEntity.rotate(player.getCell(), rotation);
                justRotated.add(rotaryEntity);
            }
        }
        moveInProgress = true;
    }
}
