package com.pigrsi.dan.gameworld;

import com.badlogic.gdx.Gdx;
import com.pigrsi.dan.GridStateListener;
import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.entityhelpers.MoveReceipt;

import java.util.*;

public class Grid {
    private Cell[][] cells;
    private int rows, columns;
    private GridStateListener listener;

    Grid(int rows, int columns) {
        cells = new Cell[rows][columns];
        for (int i = 0; i < rows; ++i) {
            cells[i] = new Cell[columns];
            for (int j = 0; j < columns; ++j)
                cells[i][j] = new Cell(i, j);
        }
        this.rows = rows;
        this.columns = columns;
    }

    public Cell getCell(int row, int column) {
        if ((row < 0 || row >= rows) || (column < 0 || column >= columns)) {
            Gdx.app.debug("Grid", "Tried to access cell that doesn't exist");
            Cell deadCell = new Cell(row, column);
            deadCell.becomeDeathCell();
            return deadCell;
        }
        return cells[row][column];
    }

    public void add(Entity entity, int row, int column) {
        add(entity, getCell(row, column));
    }

    public void add(Entity entity, Cell newCell) {
        if (newCell == null)
            Gdx.app.debug("Grid", "Tried to add to null Cell");
        else
            newCell.add(entity);
        entity.setCell(newCell, false);
    }

    public MoveReceipt requestMove(Entity entity, Entity.Direction direction, Entity.MoveType moveType) {
        int rowOffset = getRowOffset(direction);
        int columnOffset = getColumnOffset(direction);

        Set<Entity> entitiesToBePushed = new HashSet<>();

        boolean requestGranted = true;
        for (Entity linkedEntity : entity.getGroup()) {
            if (!checkMoveAllowed(linkedEntity, entitiesToBePushed, columnOffset, rowOffset, moveType))
                requestGranted = false;
        }
        requestGranted = requestGranted && checkMoveAllowed(entity, entitiesToBePushed, columnOffset, rowOffset, moveType);

        return requestGranted ? new MoveReceipt(entity, moveType, rowOffset, columnOffset, entitiesToBePushed) : null;
    }

    public void performMove(MoveReceipt move) {
        if (move == null) return;
        onGridStateChange(move.getEntity());
        for (Entity e: move.getEntitiesToBePushed()) {
            Cell currentCell = e.getCell();
            Cell newCell = getCell(currentCell.getRow() + move.getRowOffset(), currentCell.getColumn() + move.getColumnOffset());
            e.move(move.getRowOffset(), move.getColumnOffset(), move.getMoveType(), newCell);
        }
    }

    private boolean checkMoveAllowed(Entity entity, Set<Entity> entitiesToBePushed, int columnOffset, int rowOffset, Entity.MoveType moveType) {
        Cell nextCell = getCell(entity.getCell().getRow() + rowOffset, entity.getCell().getColumn() + columnOffset);
        List<Entity> pushingEntities = new ArrayList<>();
        entitiesToBePushed.add(entity);
        pushingEntities.add(entity);
        if (entitiesAreBlocked(moveType, nextCell, pushingEntities)) return false;
        while (true) {
            if (nextCell.isDeathCell()) return true;

            pushingEntities = getNextPushingEntities(pushingEntities, nextCell, moveType);

            // Check if one of the entities is blocked
            if (entitiesAreBlocked(moveType, nextCell, pushingEntities)) return false;

            for (Entity pushingEntity: pushingEntities) {
                for (Entity linkedEntity : pushingEntity.getGroup()) {
                    if (entitiesToBePushed.contains(linkedEntity)) continue;
                    if (!checkMoveAllowed(linkedEntity, entitiesToBePushed, columnOffset, rowOffset, moveType)) return false;
                }
            }

            if (pushingEntities.isEmpty()) return true;

            entitiesToBePushed.addAll(pushingEntities);
            nextCell = getCell(nextCell.getRow() + rowOffset, nextCell.getColumn() + columnOffset);
        }
    }

    private boolean entitiesAreBlocked(Entity.MoveType moveType, Cell nextCell, List<Entity> pushingEntities) {
        Set<Entity> nextCellEntities;
        nextCellEntities = nextCell.getEntities();
        for (Entity pushingEntity: pushingEntities) {
            for (Entity pushedEntity: nextCellEntities) {
                if (!pushingEntity.canPushEntity(pushedEntity, moveType) && !pushingEntity.canOverlapWithEntity(pushedEntity))
                    return true;
            }
        }
        return false;
    }

    private List<Entity> getNextPushingEntities(List<Entity> pushingEntities, Cell pushedCell, Entity.MoveType moveType) {
        Set<Entity> potentiallyPushedEntities = pushedCell.getEntities();

        List<Entity> definitelyPushedEntities = new ArrayList<>();
        for (Entity potentiallyPushedEntity: potentiallyPushedEntities) {
            for (Entity pushing: pushingEntities) {
                if (pushing.canPushEntity(potentiallyPushedEntity, moveType)) definitelyPushedEntities.add(potentiallyPushedEntity);
            }
        }

        return definitelyPushedEntities;
    }

    private void onGridStateChange(Entity instigator) {
        if (listener == null) return;
        listener.onGridStateChange(instigator);
    }

    private void moveToCell(Entity e, int rowOffset, int columnOffset) {
        Cell currentCell = e.getCell();
        currentCell.remove(e);
        Cell newCell = getCell(currentCell.getRow() + rowOffset, currentCell.getColumn() + columnOffset);
        newCell.add(e);
        e.setCell(newCell, false);
    }

    private int getRowOffset(Entity.Direction direction) {
        switch (direction) {
            case UP: return +1;
            case DOWN: return -1;
            default: return 0;
        }
    }

    private int getColumnOffset(Entity.Direction direction) {
        switch (direction) {
            case RIGHT: return +1;
            case LEFT: return -1;
            default: return 0;
        }
    }

    public void addGridStateListener(GridStateListener listener) {this.listener = listener;}

    public int getNumRows() {
        return rows;
    }

    public int getNumColumns() {
        return columns;
    }
}