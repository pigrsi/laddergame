package com.pigrsi.dan.gameworld;

enum WorldEventType {
    MOVE_IN_PROGRESS,
    MOVE_NOT_IN_PROGRESS
}

public class WorldEvent {
    public final WorldEventType type;

    public WorldEvent(WorldEventType type) {
        this.type = type;
    }
}
