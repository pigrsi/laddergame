package com.pigrsi.dan.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.pigrsi.dan.entities.block.Balloon;
import com.pigrsi.dan.entities.types.Entity;

public class MapEntitySpawner {

    public void spawnEntitiesOnLayer(EntityHome entityHome, Grid grid, TiledMapTileLayer layer) {
        if (layer == null) { Gdx.app.error(getClass().getSimpleName(), "Null layer in spawnEntitiesOnLayer method."); return; }

        for (int row = 0; row < layer.getHeight(); ++row) {
            for (int column = 0; column < layer.getWidth(); ++column) {
                TiledMapTileLayer.Cell cell = layer.getCell(column, row);
                if (cell != null) {
                    TiledMapTile tile = cell.getTile();
                    spawnEntityByTileId(entityHome, grid, tile.getId(), row, column);
                }
            }
        }
    }

    private Entity spawnEntityByTileId(EntityHome entityHome, Grid grid, int id, int x, int y) {
        switch (id) {
            case 1: return entityHome.spawnPlayer(grid, x, y);
            case 2: return entityHome.spawnBlock(grid, x, y);
            case 3: return entityHome.spawnLadder(grid, Entity.Orientation.UP, x, y);
            case 4: return entityHome.spawnPowerBlock(grid, x, y);
            case 5: return entityHome.spawnPowerLadder(grid, Entity.Orientation.UP, x, y);
            case 6: return entityHome.spawnLadder(grid, Entity.Orientation.RIGHT, x, y);
            case 7: return entityHome.spawnLadder(grid, Entity.Orientation.DOWN, x, y);
            case 8: return entityHome.spawnLadder(grid, Entity.Orientation.LEFT, x, y);
            case 9: return entityHome.spawnPowerLadder(grid, Entity.Orientation.RIGHT, x, y);
            case 10: return entityHome.spawnPowerLadder(grid, Entity.Orientation.DOWN, x, y);
            case 11: return entityHome.spawnPowerLadder(grid, Entity.Orientation.LEFT, x, y);
            case 12: return entityHome.spawnThruBlock(grid, x, y);
            case 13: return entityHome.spawnCrapLadder(grid, Entity.Orientation.UP, x, y);
            case 14: return entityHome.spawnCrapLadder(grid, Entity.Orientation.RIGHT, x, y);
            case 15: return entityHome.spawnCrapLadder(grid, Entity.Orientation.DOWN, x, y);
            case 16: return entityHome.spawnCrapLadder(grid, Entity.Orientation.LEFT, x, y);
            case 17: return entityHome.spawnSpike(grid, x, y);
            case 18: return entityHome.spawnBalloon(grid, x, y, Balloon.BalloonColour.RED);
            case 19: return entityHome.spawnBalloon(grid, x, y, Balloon.BalloonColour.BLUE);
            case 20: return entityHome.spawnBalloon(grid, x, y, Balloon.BalloonColour.GREEN);
            case 21: return entityHome.spawnStickyBlock(grid, x, y);
            default: Gdx.app.log(getClass().getSimpleName(), "Tile ID does not correspond to an entity.");
        }
        return null;
    }

}
