package com.pigrsi.dan.gameworld;

import com.pigrsi.dan.input.InputListener;

public class GameInput implements InputListener {

    private EntityManager entityManager;

    public GameInput(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public boolean inputUp() {
        return entityManager.inputUp();
    }

    @Override
    public boolean inputDown() {
        return entityManager.inputDown();
    }

    @Override
    public boolean inputLeft() {
        return entityManager.inputLeft();
    }

    @Override
    public boolean inputRight() {
        return entityManager.inputRight();
    }

    @Override
    public boolean inputEnter() {
        return entityManager.inputGrab();
    }

    @Override
    public boolean inputBackspace() {
        return entityManager.undoFromPlayer();
    }

    @Override
    public boolean inputShiftRight() {
        return entityManager.switchMoveMode();
    }

    @Override
    public boolean inputControlLeft() {
        return entityManager.rotateLeft();
    }

    @Override
    public boolean inputControlRight() {
        return entityManager.rotateRight();
    }
}
