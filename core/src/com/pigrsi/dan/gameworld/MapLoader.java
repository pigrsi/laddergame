package com.pigrsi.dan.gameworld;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.Disposable;

class MapLoader implements Disposable {

    private AssetManager manager;
    private TiledMap map;

    TiledMap loadMap() {
        manager = new AssetManager();
        manager.setLoader(TiledMap.class, new TmxMapLoader());
        manager.load("maps/toybox.tmx", TiledMap.class);
        manager.load("maps/test_1.tmx", TiledMap.class);
        manager.load("maps/test_2.tmx", TiledMap.class);

        manager.load("maps/basic/basic_0.tmx", TiledMap.class);

        manager.load("maps/spike/spike_0.tmx", TiledMap.class);
        manager.load("maps/spike/spike_1.tmx", TiledMap.class);

        manager.load("maps/linking/linking_0.tmx", TiledMap.class);
        manager.finishLoading();

        map = manager.get("maps/toybox.tmx", TiledMap.class);
        return map;
    }

    TiledMapTileLayer getLayer(TiledMap map, int layer) {
        return (TiledMapTileLayer) map.getLayers().get(layer);
    }

    @Override
    public void dispose() {
        manager.dispose();
        if (map != null) map.dispose();
    }
}
