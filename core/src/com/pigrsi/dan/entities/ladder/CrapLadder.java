package com.pigrsi.dan.entities.ladder;

import com.pigrsi.dan.animators.LadderAnimator;
import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.entityhelpers.CollisionIDs;
import com.pigrsi.dan.entityhelpers.CollisionMap;
import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.gameworld.Grid;

import java.util.List;

public class CrapLadder extends Ladder {
    public CrapLadder(Grid grid) {
        super(grid);
    }

    @Override
    public void createAnimator() {
        animator = new LadderAnimator(this, "broken_ladder.png");
    }

    @Override
    public boolean canStandOnCell(Cell cell) {return false;}

    @Override
    public int getCollisionID() {return CollisionIDs.CRAPLADDER;}

    @Override
    protected void createCollisionMap() {
        collisionMap = new CollisionMap();
    }
}
