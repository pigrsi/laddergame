package com.pigrsi.dan.entities.ladder;

import com.pigrsi.dan.entities.block.StickyBlock;
import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.entities.types.FallingEntity;
import com.pigrsi.dan.entities.misc.Player;
import com.pigrsi.dan.entities.types.Mergable;
import com.pigrsi.dan.entities.types.RotaryEntity;
import com.pigrsi.dan.entityhelpers.*;
import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.gameworld.Grid;
import com.pigrsi.dan.animators.LadderAnimator;

import java.util.ArrayList;
import java.util.List;

public class Ladder extends FallingEntity implements RotaryEntity, Mergable {

    private PersonalRotationCoach rotationCoach;
    private boolean isBottomLadder = true, isTopLadder = true;

    public Ladder(Grid grid) {
        super(grid);
        rotationCoach = new PersonalRotationCoach(this);
    }

    @Override
    public void tryMove(Direction direction) {
        if (isStill()) {
            grid.performMove(grid.requestMove(this, direction, MoveType.CARRIED));
        }
    }

    @Override
    public void move(int rowOffset, int columnOffset, MoveType moveType, Cell newCell) {
        Cell oldCell = getCell();
        super.move(rowOffset, columnOffset, moveType, newCell);

        if (moveType == MoveType.RAIL) {
            Entity player = getTouchingPlayer(oldCell);
            if (player == null && isABottomLadderOfGroup()) {
                // Check if player is monkey barrin' it
                Cell belowCell = grid.getCell(oldCell.getRow() - 1, oldCell.getColumn());
                if (belowCell != null) {
                    player = belowCell.getFirstEntityInCell(Player.class);
                }
            }
            if (player != null) {
                ((Player) player).tryRailMove(Direction.UP);
            }
        }
    }

    // Gets a player entity on same or adjacent cell
    private Player getTouchingPlayer(Cell centreCell) {
        Entity player = centreCell.getFirstEntityInCell(Player.class);
        if (player == null) player = grid.getCell(centreCell.getRow() - 1, centreCell.getColumn()).getFirstEntityInCell(Player.class);
        if (player == null) player = grid.getCell(centreCell.getRow() + 1, centreCell.getColumn()).getFirstEntityInCell(Player.class);
        if (player == null) player = grid.getCell(centreCell.getRow(), centreCell.getColumn() - 1).getFirstEntityInCell(Player.class);
        if (player == null) player = grid.getCell(centreCell.getRow(), centreCell.getColumn() + 1).getFirstEntityInCell(Player.class);
        return player == null ? null : (Player) player;
    }

    @Override
    public void onEntityMove() {
        super.onEntityMove();
        checkMerge();
    }

    @Override
    public void beforeStartFall() {
        recalculateTopAndBottomLadder();
    }

    @Override
    public boolean canPushEntity(Entity e, MoveType moveType) {
        if (e.getClass() == Player.class && shouldNotCollideWithPlayer(moveType, e)) return false;
        return super.canPushEntity(e, moveType);
    }

    private boolean shouldNotCollideWithPlayer(MoveType moveType, Entity player) {
        Cell myCell = getCell();
        return player.getCell().getColumn() != myCell.getColumn() || isHoldingMyGroup(player) || (moveType == MoveType.CLIMB);
    }

    private boolean isHoldingMyGroup(Entity player) {
        for (Entity groupEntity: group)
            if (groupEntity.getEntityGrabbingThis() == player) return true;
        return false;
    }

    @Override
    public boolean canStandOnCell(Cell cell) {
        return isABottomLadderOfGroup() && super.canStandOnCell(cell);
    }

    @Override
    public void onEndFall(FallStopper fallStopper) {
        super.onEndFall(fallStopper);
        checkMerge();
    }

    @Override
    public void onFellIntoCell(Cell newCell, Cell oldCell) {
        checkMerge();
    }

    @Override
    public void checkMerge() {
        Cell myCell = getCell();
        Cell[] adjacentCells = new Cell[] {
            grid.getCell(myCell.getRow() + 1, myCell.getColumn()),
            grid.getCell(myCell.getRow() - 1, myCell.getColumn()),
            grid.getCell(myCell.getRow(), myCell.getColumn() - 1),
            grid.getCell(myCell.getRow(), myCell.getColumn() + 1)
        };

        switch (rotationCoach.orientation) {
            case UP: case DOWN:
                checkMergeLadder(adjacentCells[0]);
                if (gravityHandler.isSatisfied())
                    checkMergeLadder(adjacentCells[1]);
                break;
            case LEFT: case RIGHT:
                checkMergeLadder(adjacentCells[2]);
                checkMergeLadder(adjacentCells[3]);
                break;
        }

        for (Cell cell: adjacentCells) {
            checkMergeSticky(cell);
        }
    }

    private boolean checkMergeSticky(Cell cell) {
        StickyBlock potentialSticky = (StickyBlock) cell.getFirstEntityInCell(StickyBlock.class);
        if (potentialSticky != null && !group.contains(potentialSticky)) {
            mergeGroups(potentialSticky);
            return true;
        }
        return false;
    }

    private boolean checkMergeLadder(Cell cell) {
        Ladder potentialLadder = (Ladder) cell.getFirstEntityInCell(Ladder.class);
        if (potentialLadder != null && !group.contains(potentialLadder) &&
            orientationSameAxis(rotationCoach.orientation, potentialLadder.getOrientation())) {
            mergeGroups(potentialLadder);
            return true;
        }
        return false;
    }

    @Override
    public void onGroupChanged() {
        super.onGroupChanged();
        recalculateTopAndBottomLadder();
    }

    private boolean orientationSameAxis(Orientation a, Orientation b) {
        if (a == Orientation.DOWN || a == Orientation.UP) return b == Orientation.DOWN || b == Orientation.UP;
        else return b == Orientation.LEFT || b == Orientation.RIGHT;
    }

    // Need to avoid recalculating while falling as this can cause bugs while ladders change cells
    private void recalculateTopAndBottomLadder() {
        recalculateIsBottomLadder();
        recalculateIsTopLadder();
    }

    // Checks if this ladder is at the bottom of all ladders in that column in the group
    private void recalculateIsBottomLadder() {
        Cell myCell = getCell();
        Cell cellBelow = grid.getCell(myCell.getRow() - 1, myCell.getColumn());
        for (Entity entityBelow: cellBelow.getEntitiesInCell(Ladder.class)) {
            if (entityBelow.getClass() != CrapLadder.class && group.contains(entityBelow)) {
                isBottomLadder = false;
                return;
            }
        }
        isBottomLadder = true;
    }

    private void recalculateIsTopLadder() {
        Cell myCell = getCell();
        Cell cellAbove = grid.getCell(myCell.getRow() + 1, myCell.getColumn());
        for (Entity entityAbove: cellAbove.getEntitiesInCell(Ladder.class)) {
            if (entityAbove.getClass() != CrapLadder.class && group.contains(entityAbove)) {
                isTopLadder = false;
                return;
            }
        }
        isTopLadder = true;
    }

    public boolean isABottomLadderOfGroup() { return isBottomLadder; }

    public boolean isATopLadderOfGroup() { return isTopLadder; }

    @Override
    public Orientation getOrientation() { return rotationCoach.orientation; }

    @Override
    public void setOrientation(Orientation orientation) { rotationCoach.orientation = orientation; }

    @Override
    public void rotate(Cell about, RotationDirection rotationDirection) {
        rotationCoach.rotate(about, rotationDirection, grid);
    }

    @Override
    public void reverseLastRotation() {
        rotationCoach.reverseLastRotation(grid);
    }

    @Override
    public void onRotationSuccess() {
        checkMerge();
        recalculateTopAndBottomLadder();
    }

    @Override
    public PersonalRotationCoach getRotationCoach() {
        return rotationCoach;
    }

    @Override
    public void createAnimator() {
        animator = new LadderAnimator(this, "ladder.png");
    }

    @Override
    public int getCollisionID() {return CollisionIDs.LADDER;}

    @Override
    protected void createCollisionMap() {
        collisionMap = new CollisionMap();
        collisionMap.addCollision(true, false, false, CollisionIDs.BLOCK, CollisionIDs.POWERBLOCK, CollisionIDs.SPIKE);
        collisionMap.addCollision(true, false, true, CollisionIDs.THRUBLOCK);
        collisionMap.addCollision(false, false, true, CollisionIDs.CRAPLADDER);
        collisionMap.addCollision(true, true, false,
                CollisionIDs.LADDER, CollisionIDs.POWERLADDER, CollisionIDs.BALLOON, CollisionIDs.STICKY_BLOCK);
        collisionMap.addCollision(false, false, true, CollisionIDs.RAIL);
        collisionMap.addCollision(true, false, true, CollisionIDs.PLAYER);
    }
}
