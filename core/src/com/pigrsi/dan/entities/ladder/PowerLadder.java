package com.pigrsi.dan.entities.ladder;

import com.pigrsi.dan.animators.PowerLadderAnimator;
import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.entities.block.PowerBlock;
import com.pigrsi.dan.entities.misc.Rail;
import com.pigrsi.dan.entityhelpers.CollisionIDs;
import com.pigrsi.dan.entityhelpers.MoveReceipt;
import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.gameworld.Grid;

public class PowerLadder extends Ladder {
    public PowerLadder(Grid grid) {
        super(grid);
    }

    @Override
    public boolean checkForAutomove() {
        if (isBeingHeld()) return false;
        Cell cell = getCell();
        Cell below = grid.getCell(cell.getRow() - 1, cell.getColumn());
        Entity potentialPowerBlockBelow = below.getFirstEntityInCell(PowerBlock.class);
        if (potentialPowerBlockBelow != null) {
            PowerBlock powerBlock = (PowerBlock) potentialPowerBlockBelow;
            if (!powerBlock.railsAreDeployed()) {
                ((PowerBlock) potentialPowerBlockBelow).releaseRails();
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean shouldFall() {
        return !isOnRail();
    }

    private boolean isBeingHeld() {
        for (Entity entity: group) {
            if (entity.getEntityGrabbingThis() != null) return true;
        }
        return false;
    }

    @Override
    public void createAnimator() {
        animator = new PowerLadderAnimator(this);
    }

    // Returns whether or not the push resulted in another move
    public boolean pushedDownByEntity() {
        boolean canLift = isOnRail();
        if (!canLift) return false;

        MoveReceipt move = grid.requestMove(this, Direction.UP, MoveType.RAIL);

        if (move == null) return false;
        grid.performMove(move);
        return true;
    }

    private boolean isOnRail() {
        boolean canLift = false;
        for (Entity e: group) {
            if (e.getClass() == getClass() && e.getCell().getFirstEntityInCell(Rail.class) != null) {canLift = true; break;}
        }
        return canLift;
    }

    @Override
    public int getCollisionID() {return CollisionIDs.POWERLADDER;}
}