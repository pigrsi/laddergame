package com.pigrsi.dan.entities.block;

import com.pigrsi.dan.entities.types.StaticEntity;
import com.pigrsi.dan.entityhelpers.CollisionIDs;
import com.pigrsi.dan.gameworld.Grid;
import com.pigrsi.dan.animators.BasicAnimator;

public class Block extends StaticEntity {

    public Block(Grid grid) {
        super(grid);
    }

    @Override
    public void createAnimator() {
        animator = new BasicAnimator(this, "grey.png");
    }

    @Override
    public int getCollisionID() {return CollisionIDs.BLOCK;}
}
