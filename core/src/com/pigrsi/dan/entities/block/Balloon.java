package com.pigrsi.dan.entities.block;

import com.pigrsi.dan.animators.BalloonAnimator;
import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.entities.types.Mergable;
import com.pigrsi.dan.entities.types.MovableEntity;
import com.pigrsi.dan.entities.types.RotaryEntity;
import com.pigrsi.dan.entityhelpers.CollisionIDs;
import com.pigrsi.dan.entityhelpers.CollisionMap;
import com.pigrsi.dan.entityhelpers.EntityUtil;
import com.pigrsi.dan.entityhelpers.PersonalRotationCoach;
import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.gameworld.Grid;

public class Balloon extends MovableEntity implements Mergable, RotaryEntity {
    public BalloonColour colour;
    public PersonalRotationCoach rotationCoach;

    public enum BalloonColour {
        RED, BLUE, GREEN
    }

    public Balloon(Grid grid) {
        super(grid);
        rotationCoach = new PersonalRotationCoach(this);
    }

    public void setColour(BalloonColour colour) {
        this.colour = colour;
        ((BalloonAnimator) animator).colourChanged();
    }

    @Override
    protected void createCollisionMap() {
        collisionMap = new CollisionMap();
        collisionMap.addCollision(true, true, false,
                CollisionIDs.BALLOON, CollisionIDs.LADDER, CollisionIDs.POWERLADDER, CollisionIDs.PLAYER, CollisionIDs.STICKY_BLOCK);
        collisionMap.addCollision(true, false, false, CollisionIDs.BLOCK, CollisionIDs.POWERBLOCK);
        collisionMap.addCollision(false, false, true, CollisionIDs.SPIKE, CollisionIDs.THRUBLOCK);
    }

    @Override
    public void checkMerge() {
        Cell[] mergableCells = EntityUtil.getAdjacentCells(getCell(), grid);

        for (int i = 0; i < mergableCells.length; ++i) {
            Cell cell = mergableCells[i];
            Entity entity = cell.getFirstEntityInCell(Balloon.class);
            if (entity != null) {
                Balloon balloon = (Balloon) entity;
                if (!group.contains(balloon) && balloon.colour == colour) mergeGroups(balloon);
            }
        }
    }

    @Override
    public Orientation getOrientation() {
        return rotationCoach.orientation;
    }

    @Override
    public void setOrientation(Orientation orientation) {
        rotationCoach.orientation = orientation;
    }

    @Override
    public void rotate(Cell about, RotationDirection direction) {
        rotationCoach.rotate(about, direction, grid);
    }

    @Override
    public void reverseLastRotation() {
        rotationCoach.reverseLastRotation(grid);
    }

    @Override
    public void onRotationSuccess() { }

    @Override
    public PersonalRotationCoach getRotationCoach() {
        return rotationCoach;
    }

    @Override
    public void createAnimator() {
        animator = new BalloonAnimator(this, "red_balloon.png");
    }

    @Override
    public int getCollisionID() {
        return CollisionIDs.BALLOON;
    }
}
