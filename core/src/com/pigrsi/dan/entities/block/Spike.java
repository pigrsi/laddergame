package com.pigrsi.dan.entities.block;

import com.pigrsi.dan.animators.BasicAnimator;
import com.pigrsi.dan.entities.types.StaticEntity;
import com.pigrsi.dan.entityhelpers.CollisionIDs;
import com.pigrsi.dan.gameworld.Grid;

public class Spike extends StaticEntity {

    public Spike(Grid grid) { super(grid); }

    @Override
    public void createAnimator() {
        animator = new BasicAnimator(this, "spike.png");
    }

    @Override
    public int getCollisionID() {
        return CollisionIDs.SPIKE;
    }
}
