package com.pigrsi.dan.entities.block;

import com.pigrsi.dan.animators.BasicAnimator;
import com.pigrsi.dan.entities.misc.Rail;
import com.pigrsi.dan.entityhelpers.CollisionIDs;
import com.pigrsi.dan.gameworld.Grid;

import java.util.List;

public class PowerBlock extends Block {

    private List<Rail> rails;
    boolean railsDeployed;

    public PowerBlock(Grid grid) {
        super(grid);
    }

    public void setRails(List<Rail> rails) {
        this.rails = rails;
        for (Rail rail: rails) {
            rail.setCell(null, true);
        }
    }

    @Override
    public void createAnimator() {
        animator = new BasicAnimator(this, "rainbow_block.png");
    }

    public void releaseRails() {
        for (int i = 0; i < rails.size(); ++i) rails.get(i).moveUpFromCell(getCell(), i + 1);
        railsDeployed = true;
    }

    public boolean railsAreDeployed() { return railsDeployed; }

    public void setRailsDeployed(boolean set) { railsDeployed = set; }

    @Override
    public int getCollisionID() {return CollisionIDs.POWERBLOCK;}
}
