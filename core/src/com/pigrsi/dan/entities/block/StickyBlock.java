package com.pigrsi.dan.entities.block;

import com.pigrsi.dan.animators.BasicAnimator;
import com.pigrsi.dan.entities.ladder.Ladder;
import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.entities.types.FallingEntity;
import com.pigrsi.dan.entities.types.Mergable;
import com.pigrsi.dan.entities.types.RotaryEntity;
import com.pigrsi.dan.entityhelpers.*;
import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.gameworld.Grid;

public class StickyBlock extends FallingEntity implements Mergable, RotaryEntity {
    private PersonalRotationCoach rotationCoach;

    public StickyBlock(Grid grid) {
        super(grid);
        rotationCoach = new PersonalRotationCoach(this);
    }

    @Override
    public void onEntityMove() {
        super.onEntityMove();
        checkMerge();
    }

    @Override
    public void onEndFall(FallStopper fallStopper) {
        super.onEndFall(fallStopper);
        checkMerge();
    }

    @Override
    public Orientation getOrientation() {
        return rotationCoach.orientation;
    }

    @Override
    public void setOrientation(Orientation orientation) {
        rotationCoach.orientation = orientation;
    }

    @Override
    public void rotate(Cell about, RotationDirection direction) {
        rotationCoach.rotate(about, direction, grid);
    }

    @Override
    public void reverseLastRotation() {
        rotationCoach.reverseLastRotation(grid);
    }

    @Override
    public void onRotationSuccess() {
        checkMerge();
    }

    @Override
    public PersonalRotationCoach getRotationCoach() {
        return rotationCoach;
    }

    @Override
    public void createAnimator() {
        animator = new BasicAnimator(this, "sticky_block.png");
    }

    @Override
    public int getCollisionID() {
        return CollisionIDs.STICKY_BLOCK;
    }

    @Override
    protected void createCollisionMap() {
        collisionMap = new CollisionMap();
        collisionMap.addCollision(true, true, false,
                CollisionIDs.BALLOON, CollisionIDs.STICKY_BLOCK, CollisionIDs.LADDER, CollisionIDs.POWERLADDER, CollisionIDs.PLAYER);
        collisionMap.addCollision(true, false, false, CollisionIDs.BLOCK, CollisionIDs.POWERBLOCK, CollisionIDs.SPIKE);
        collisionMap.addCollision(true, false, true, CollisionIDs.THRUBLOCK);
    }

    @Override
    public void checkMerge() {
        Cell[] mergableCells = EntityUtil.getAdjacentCells(getCell(), grid);

        for (int i = 0; i < mergableCells.length; ++i) {
            Cell cell = mergableCells[i];
            for (Entity entity: cell.getEntities()) {
                if (entity instanceof Ladder || entity instanceof Balloon || entity instanceof StickyBlock) {
                    if (!group.contains(entity)) mergeGroups(entity);
                }
            }
        }
    }

    @Override
    public void onFellIntoCell(Cell newCell, Cell oldCell) {
        checkMerge();
    }
}
