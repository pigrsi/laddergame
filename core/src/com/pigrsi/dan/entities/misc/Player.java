package com.pigrsi.dan.entities.misc;

import com.badlogic.gdx.Gdx;
import com.pigrsi.dan.entities.ladder.CrapLadder;
import com.pigrsi.dan.entities.ladder.Ladder;
import com.pigrsi.dan.entities.ladder.PowerLadder;
import com.pigrsi.dan.entities.block.Spike;
import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.entities.types.FallingEntity;
import com.pigrsi.dan.entityhelpers.*;
import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.gameworld.Grid;
import com.pigrsi.dan.animators.BasicAnimator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Player extends FallingEntity {

    private Entity grabbedEntity;
    public enum MoveMode {
        PLAYER, PLAYER_GRABBED
    }
    public MoveMode moveMode = MoveMode.PLAYER;
    public MoveMode lastMoveType = MoveMode.PLAYER;
    public ClimbingMap climbingMap;

    public Player(Grid grid) {
        super(grid);
    }

    @Override
    public void update(float delta) {
        super.update(delta);
    }

    @Override
    public boolean checkForAutomove() {
        boolean moveMade = false;
        Cell myCell = getCell();
        List<Cell> checkCells = new ArrayList<>();
        if (previousFallStopper == FallStopper.CLIMB) {
            checkCells.add(myCell);
            checkCells.add(grid.getCell(myCell.getRow() + 1, myCell.getColumn()));
            checkCells.add(grid.getCell(myCell.getRow(), myCell.getColumn() - 1));
            checkCells.add(grid.getCell(myCell.getRow(), myCell.getColumn() + 1));
        } else if (previousFallStopper == FallStopper.FLOOR) {
            checkCells.add(grid.getCell(myCell.getRow() - 1, myCell.getColumn()));
        }

        boolean _break = false;
        if (!checkCells.isEmpty()) {
            for (Cell cell: checkCells) {
                List<Entity> laddersInCell = cell.getEntitiesInCell(Ladder.class);
                Ladder ladder = !laddersInCell.isEmpty() ? (Ladder) laddersInCell.get(0) : null;
                if (ladder != null) {
                    for (Entity entity: ladder.getGroup()) {
                        if (entity instanceof PowerLadder) {
                            moveMade = ((PowerLadder) entity).pushedDownByEntity() || moveMade;
                            _break = true;
                            break;
                        }
                    }
                    if (_break) break;
                }
            }
        }
        moveMade = checkShouldDropEntity() || moveMade;
        return moveMade;
    }

    @Override
    public void endFallOnCurrentCell() {
        Cell currentCell = getCell();
        Cell cellBelow = grid.getCell(currentCell.getRow() - 1, currentCell.getColumn());
        FallStopper stopper = EntityUtil.landingShouldCountAsClimb(grid, cellBelow, this) ? FallStopper.CLIMB : FallStopper.FLOOR;
        gravityHandler.endFall(getCell(), stopper);
    }

    private boolean checkShouldDropEntity() {
        if (isHoldingSomething()) {
            Cell myCell = getCell();
            Entity holding = getGrabbedEntity();
            Entity overlapping = getClimbableEntityInCell(myCell);
            Cell cellBelow = grid.getCell(myCell.getRow() - 1, myCell.getColumn());
            Entity below = getClimbableEntityInCell(cellBelow);
            if (!EntityUtil.playerIsTouchingALadderInGroup(this, holding) ||
                EntityUtil.playerIsAboveAllLaddersInGroup(this, holding) ||
                (overlapping != null && holding.getGroup().contains(overlapping) && below != null && holding.getGroup().contains(below) && !canStandOnCell(cellBelow))
            ) {
                dropEntity();
                this.setMoveMode(Player.MoveMode.PLAYER);
                return true;
            }
        }
        return false;
    }

    private Entity getClimbableEntityInCell(Cell cell) {
        for (Entity entity: cell.getEntities()) if (canClimbEntity(entity)) return entity;
        return null;
    }

    @Override
    public void onEntityMove() {
        super.onEntityMove();
    }

    @Override
    public void onGroupChanged() {
        if (getGrabbedEntity() == null) return;
        Entity grabbed = getGrabbedEntity();
        Cell myCell = getCell();
        Cell cellBelow = grid.getCell(myCell.getRow() - 1, myCell.getColumn());

        for (Entity entityBelow: cellBelow.getEntities()) {
            if (grabbed.getGroup().contains(entityBelow)) continue;
            if (canStandOnEntity(entityBelow)) return;
        }
        dropEntity();
    }

    @Override
    public void tryMove(Direction direction) {
        lastMoveType = moveMode;
        if (moveMode == MoveMode.PLAYER_GRABBED && getGrabbedEntity() != null) {
            getGrabbedEntity().tryMove(direction);
            return;
        }
        switch (direction) {
            case UP:
                tryMoveUp();
                break;
            case DOWN:
                tryMoveDown();
                break;
            default:
                super.tryMove(direction);
        }
    }

    private void tryMoveUp() {
        if (!isStill()) return;
        MoveType moveType = MoveType.JUMP;
        if (canClimbUp(getCell())) moveType = MoveType.CLIMB;
        MoveReceipt playerMove = grid.requestMove(this, Entity.Direction.UP, moveType);
        if (playerMove == null) return;
        if (getGrabbedEntity() != null) grid.performMove(grid.requestMove(getGrabbedEntity(), Entity.Direction.UP, moveType));
        grid.performMove(playerMove);
    }

    private void tryMoveDown() {
        if (!isStill()) return;
        MoveReceipt playerMove = grid.requestMove(this, Entity.Direction.DOWN, MoveType.CLIMB);
        if (playerMove == null) return;
        if (getGrabbedEntity() != null) grid.performMove(grid.requestMove(getGrabbedEntity(), Entity.Direction.DOWN, MoveType.CLIMB));
        grid.performMove(playerMove);
    }

    public void tryRailMove(Direction direction) {
        Cell myCell = getCell();
        MoveReceipt playerMove = grid.requestMove(this, direction, MoveType.RAIL);
        if (playerMove == null) return;
        grid.performMove(playerMove);
    }

    private boolean canClimbUp(Cell playerCell) {
        if (!canClimb()) return false;
        if (previousFallStopper == FallStopper.FLOOR) return canClimbOnCell(playerCell);
        if (previousFallStopper == FallStopper.CLIMB)
            return canClimbOnCell(playerCell) ||
                    canClimbOnCell(grid.getCell(playerCell.getRow(), playerCell.getColumn() - 1)) ||
                    canClimbOnCell(grid.getCell(playerCell.getRow(), playerCell.getColumn() + 1)) ||
                    canClimbOnCell(grid.getCell(playerCell.getRow() + 1, playerCell.getColumn()));
        return false;
    }

    private boolean canClimbDown(Cell playerCell) {
        if (!canClimb()) return false;
        return canClimbOnCell(playerCell)
                || canClimbOnCell(grid.getCell(playerCell.getRow(), playerCell.getColumn() - 1))
                || canClimbOnCell(grid.getCell(playerCell.getRow(), playerCell.getColumn() + 1))
                || canClimbOnCell(grid.getCell(playerCell.getRow() - 1, playerCell.getColumn()))
                || canClimbOnCell(grid.getCell(playerCell.getRow() - 1, playerCell.getColumn() - 1))
                || canClimbOnCell(grid.getCell(playerCell.getRow() - 1, playerCell.getColumn() + 1));
    }

    @Override
    public boolean canPushEntity(Entity e, MoveType moveType) {
        if (e instanceof Ladder) {
            if (e instanceof CrapLadder) return false;
            Ladder ladder = (Ladder) e;
            Cell myCell = getCell();
            Cell ladderCell = ladder.getCell();
            for (Entity cellEntity: myCell.getEntities())
                if (ladder.getGroup().contains(cellEntity)) return false;
                if (moveType == MoveType.JUMP && EntityUtil.groupIsStandingOnOnlyMe(ladder, this, grid)) return true;
            if (ladder.isABottomLadderOfGroup() &&
                    myCell.getColumn() == ladderCell.getColumn() &&
                    myCell.getRow() == ladderCell.getRow() - 1 &&
                    (canClimbOnCell(grid.getCell(myCell.getRow(), myCell.getColumn() - 1)) || canClimbOnCell(grid.getCell(myCell.getRow(), myCell.getColumn() + 1))))
                return true;
        }
        return super.canPushEntity(e, moveType);
    }

    public void grabEntity(Entity entity) { this.grabbedEntity = entity;
        entity.onGrabbedByEntity(this);
        Gdx.app.log(getClass().getSimpleName(), "Player has grabbed entity of type " + entity.getClass());
    }

    public boolean isHoldingSomething() { return grabbedEntity != null; }

    public boolean isHoldingEntity(Entity entity) { return entity != null && grabbedEntity == entity; }

    @Override
    public Entity getGrabbedEntity() {return grabbedEntity;}

    @Override
    public void dropEntity() {
        if (grabbedEntity == null) return;
        for (Entity groupEntity: grabbedEntity.getGroup())
            groupEntity.onDroppedByEntity();
        grabbedEntity = null;
        Gdx.app.log(getClass().getSimpleName(), "Player has dropped entity.");
    }

    public void setMoveMode(MoveMode mode) {
        moveMode = mode;
        Gdx.app.log(getClass().getSimpleName(), "Set moveMode to " + mode);
    }

    public MoveMode getMoveMode() {return moveMode;}

    @Override
    public void onEndFall(FallStopper fallStopper) {
        Gdx.app.log(getClass().getSimpleName(), "Entity ended fall by " + fallStopper);
        super.onEndFall(fallStopper);
        if (isHoldingSomething() && (fallStopper == FallStopper.CLIMB || fallStopper == FallStopper.GRAB)) {
            if (!canGrabEntity(grabbedEntity)) {
                dropEntity();
                gravityHandler.startFall();
            }
        }
    }

    public boolean canGrabEntity(Entity grabbed) {
        Cell pickerCell = getCell();
        Cell cellBelow = grid.getCell(pickerCell.getRow() - 1, pickerCell.getColumn());
        Set<Entity> grabbedGroup = grabbed.getGroup();

        // Check that player is not using the grabbed entity to stand on
        List<Entity> possibleEntitiesStoodOn = new ArrayList<>();
        for (Entity entityBelow: cellBelow.getEntities())
            if (!grabbedGroup.contains(entityBelow)) possibleEntitiesStoodOn.add(entityBelow);

        for (Entity potentialFloor: possibleEntitiesStoodOn) {
            if (canStandOnEntity(potentialFloor)) return true;
        }
        return false;
    }

    @Override
    public void setCell(Cell cell, boolean resetPosition) {
        super.setCell(cell, resetPosition);
        if (getCell().getFirstEntityInCell(Spike.class) != null) setAlive(false);
    }

    @Override
    public boolean canClimb() {
        return !isHoldingSomething();
    }

    @Override
    public boolean canClimbOnCell(Cell cell) {
        for (Entity entity: cell.getEntities()) {
            if (entity.isStill() && climbingMap.canClimb(entity) && !isHoldingEntity(entity)) return true;
        }
        return false;
    }

    @Override
    public boolean canStandOnEntity(Entity entity) {
        if (entity instanceof Ladder) {
            return super.canStandOnEntity(entity) && ((Ladder) entity).isATopLadderOfGroup();
        } else {
            return super.canStandOnEntity(entity);
        }
    }

    @Override
    public boolean canOverlapWithEntity(Entity e) {
        if (e instanceof Spike) {
            return getCell().getRow() > e.getCell().getRow();
        } else {
            return super.canOverlapWithEntity(e);
        }
    }

    @Override
    public boolean canClimbEntity(Entity e) {
        return this != e && e.isStill() && climbingMap.canClimb(e);
    }

    @Override
    public void createAnimator() {
        animator = new BasicAnimator(this, "brown.png", 48, 48);
    }

    @Override
    protected void createCollisionMap() {
        collisionMap = new CollisionMap();
        collisionMap.addCollision(true, false, false, CollisionIDs.BLOCK, CollisionIDs.POWERBLOCK);
        collisionMap.addCollision(true, false, true, CollisionIDs.THRUBLOCK);
        collisionMap.addCollision(false, false, true, CollisionIDs.CRAPLADDER);
        collisionMap.addCollision(true, false, true, CollisionIDs.LADDER, CollisionIDs.POWERLADDER);
        collisionMap.addCollision(false, false, true, CollisionIDs.RAIL);
        collisionMap.addCollision(false, false, true, CollisionIDs.SPIKE);
        collisionMap.addCollision(true, true, false, CollisionIDs.BALLOON, CollisionIDs.STICKY_BLOCK);

        climbingMap = new ClimbingMap();
        climbingMap.addClimbable(CollisionIDs.LADDER, CollisionIDs.POWERLADDER, CollisionIDs.CRAPLADDER);
    }

    @Override
    public int getCollisionID() {return CollisionIDs.PLAYER;}

    @Override
    public void onFellIntoCell(Cell newCell, Cell oldCell) { }
}
