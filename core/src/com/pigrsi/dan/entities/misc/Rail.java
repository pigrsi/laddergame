package com.pigrsi.dan.entities.misc;

import com.pigrsi.dan.animators.RailAnimator;
import com.pigrsi.dan.entities.types.MovableEntity;
import com.pigrsi.dan.entityhelpers.CollisionIDs;
import com.pigrsi.dan.entityhelpers.SlideMover;
import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.gameworld.Grid;

public class Rail extends MovableEntity {
    public Rail(Grid grid) {
        super(grid);
    }

    @Override
    public void createAnimator() {
        animator = new RailAnimator(this);
    }

    public void moveUpFromCell(Cell newCell, int i) {
        setCell(newCell, true);
        Cell destinationCell = grid.getCell(newCell.getRow() + i, newCell.getColumn());
        setCell(destinationCell, false);
        entityMover = new SlideMover(this, destinationCell.getWorldX(), destinationCell.getWorldY());
        entityMover.setLifetime(1.125f);
    }

    @Override
    public boolean isAlive() { return true; }

    @Override
    public int getCollisionID() {return CollisionIDs.RAIL;}
}
