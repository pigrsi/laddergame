package com.pigrsi.dan.entities.types;

import com.pigrsi.dan.entityhelpers.PersonalRotationCoach;
import com.pigrsi.dan.gameworld.Cell;

public interface RotaryEntity extends Entity {
    Orientation getOrientation();
    void setOrientation(Orientation orientation);
    void rotate(Cell about, Entity.RotationDirection direction);
    void reverseLastRotation();
    void onRotationSuccess();
    PersonalRotationCoach getRotationCoach();
}
