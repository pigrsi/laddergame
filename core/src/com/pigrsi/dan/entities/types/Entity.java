package com.pigrsi.dan.entities.types;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;
import com.pigrsi.dan.animators.Animator;
import com.pigrsi.dan.entityhelpers.EntityMover;
import com.pigrsi.dan.gameworld.Cell;

import java.util.Set;

public interface Entity extends Disposable {
    enum MoveType {
        JUMP, SLIDE, CLIMB, CARRIED, RAIL
    }
    enum Direction {
        UP, DOWN, LEFT, RIGHT
    }
    enum FallStopper {
        FLOOR, CLIMB, GROUP, GRAB
    }
    enum RotationDirection {
        CLOCKWISE, ANTICLOCKWISE
    }
    enum Orientation {
        UP, DOWN, LEFT, RIGHT
    }
    void update(float delta);
    void postUpdate(float delta);
    void render(SpriteBatch batch);
    void tryMove(Direction direction);
    void onEntityMove();
    void onGrabbedByEntity(Entity grabber);
    void onDroppedByEntity();
    void onGroupChanged();
    void dropEntity();
    void move(int rowOffset, int columnOffset, MoveType moveType, Cell newCell);
    void setPosition(float x, float y);
    void setCell(Cell newCell, boolean resetPosition);
    void mergeGroups(Entity entity);
    void setGroup(Set<Entity> group);
    void onEndFall(FallStopper fallStopper);
    void setPreviousFallStopper(FallStopper fallStopper);
    void setName(String name);
    void setAlive(boolean alive);
    void createAnimator();
    void setEntityMover(EntityMover mover);
    void beforeStartFall();
    String getName();
    Cell getCell();
    Set<Entity> getGroup();
    FallStopper getPreviousFallStopper();
    Entity getGrabbedEntity();
    Entity getEntityGrabbingThis();
    Animator getAnimator();
    int getCollisionID();
    boolean canStandOnEntity(Entity e);
    boolean canPushEntity(Entity e, MoveType moveType);
    boolean canOverlapWithEntity(Entity e);
    boolean canClimbEntity(Entity e);
    boolean canClimb();
    boolean canStandOnCell(Cell cell);
    boolean canClimbOnCell(Cell cell);
    boolean canOverlapOnCell(Cell cell);
    boolean canPushCell(Cell cell, MoveType moveType);
    boolean isStill();
    boolean isAlive();
    boolean checkForAutomove();
    float getX();
    float getY();
}
