package com.pigrsi.dan.entities.types;

public interface Mergable {
    void checkMerge();
}
