package com.pigrsi.dan.entities.types;

import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.gameworld.Grid;
import com.pigrsi.dan.entityhelpers.GravityHandler;

public abstract class FallingEntity extends MovableEntity {
    protected GravityHandler gravityHandler;
    private boolean updateGravityHandler = true;

    public FallingEntity(Grid grid) {
        super(grid);
        gravityHandler = new GravityHandler(this, grid);
        gravityHandler.startFall();
    }

    public abstract void onFellIntoCell(Cell newCell, Cell oldCell);

    @Override
    public void update(float delta) {
        super.update(delta);
        if (updateGravityHandler && !gravityHandler.isSatisfied() && !entityMoverActive()) gravityHandler.update();
    }

    @Override
    public void postUpdate(float delta) {
        if (!gravityHandler.isSatisfied() && !entityMoverActive()) gravityHandler.checkGroupIsSatisfied();
    }


    @Override
    public void onEntityMove() {
        super.onEntityMove();
        if (shouldFall()) {
            beforeStartFall();
            gravityHandler.startFall();
        }
    }

    @Override
    public void onDroppedByEntity() {
        super.onDroppedByEntity();
        if (shouldFall()) {
            beforeStartFall();
            gravityHandler.startFall();
        }
    }

    @Override
    public void onEndFall(FallStopper fallStopper) {
        super.onEndFall(fallStopper);
        Cell myCell = getCell();
        Cell cellAbove = grid.getCell(myCell.getRow() + 1, myCell.getColumn());
        for (FallingEntity entityAbove: cellAbove.getFallingEntities()) {
            if (entityAbove.canStandOnCell(entityAbove.getCell())) entityAbove.endFallOnCurrentCell();
        }
    }

    public void endFallOnCurrentCell() {
        gravityHandler.endFall(getCell(), FallStopper.FLOOR);
    }

    public boolean shouldFall() {
        return true;
    }

    @Override
    public boolean isStill() {
        return super.isStill() && gravityHandler.isSatisfied();
    }

    @Override
    public boolean canStandOnCell(Cell cell) {
        for (Entity entity: cell.getEntities()) if (canStandOnEntity(entity)) return true;
        return false;
    }
}
