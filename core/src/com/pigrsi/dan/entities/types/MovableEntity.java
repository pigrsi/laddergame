package com.pigrsi.dan.entities.types;

import com.badlogic.gdx.Gdx;
import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.gameworld.Grid;
import com.pigrsi.dan.gameworld.World;
import com.pigrsi.dan.entityhelpers.*;

public abstract class MovableEntity extends BasicEntity {
    private float x, y;
    private Cell cell;
    private Entity grabbedBy;
    protected EntityMover entityMover;
    protected CollisionMap collisionMap;

    public MovableEntity(Grid grid) {
        super(grid);
        createCollisionMap();
    }

    @Override
    public void update(float delta) {
        if (entityMover != null) {
            entityMover.update(delta);
            if (entityMover.isDone()) entityMover = null;
        }
    }

    @Override
    public void tryMove(Direction direction) {
        if (isStill()) {
            MoveReceipt myMove = grid.requestMove(this, direction, MoveType.SLIDE);
            if (myMove == null) return;
            if (getGrabbedEntity() != null) {
                MoveReceipt grabbedMove = grid.requestMove(getGrabbedEntity(), direction, MoveType.SLIDE);
                if (grabbedMove != null) {
                    grid.performMove(grabbedMove);
                    grid.performMove(myMove);
                }
            } else {
                grid.performMove(myMove);
            }
        }
    }

    @Override
    public void move(int rowOffset, int columnOffset, MoveType moveType, Cell newCell) {
        if (!isStill()) return;

        switch (moveType) {
            case JUMP:
                entityMover = new JumpMover(this, x + columnOffset * World.CELL_SIZE, y + rowOffset * World.CELL_SIZE);
                break;
            case SLIDE: case CLIMB: case CARRIED:
                entityMover = new SlideMover(this, x + columnOffset * World.CELL_SIZE, y + rowOffset * World.CELL_SIZE);
                break;
            case RAIL:
                entityMover = new RailMover(this, x + columnOffset * World.CELL_SIZE, y + rowOffset * World.CELL_SIZE);
                break;
            default:
                Gdx.app.log(getClass().getSimpleName(), "No moveType set, using RailMover.");
                entityMover = new RailMover(this, x + columnOffset * World.CELL_SIZE, y + rowOffset * World.CELL_SIZE);
                break;
        }
        setCell(newCell, false);
    }

    @Override
    public Entity getGrabbedEntity() { return null; }

    @Override
    public Entity getEntityGrabbingThis() { return grabbedBy; }

    @Override
    public void onGrabbedByEntity(Entity grabber) { this.grabbedBy = grabber; }

    @Override
    public void onDroppedByEntity() { this.grabbedBy = null; }

    @Override
    public boolean isStill() {
        return entityMover == null;
    }

    @Override
    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void setCell(Cell newCell, boolean resetPosition) {
        if (newCell != null) {
            if (newCell.isDeathCell()) resetPosition = false;
            if (cell == null || resetPosition) {
                // Snap position to cell if I wasn't in one previously
                setPosition(newCell.getWorldX(), newCell.getWorldY());
            }
        }

        if (cell != null) cell.remove(this);
        if (newCell != null) newCell.add(this);

        cell = newCell;
    }

    protected void createCollisionMap() { collisionMap = new CollisionMap(); }

    @Override
    public boolean canStandOnEntity(Entity e) {
        return this != e && e.isStill() && collisionMap.canStandOn(e);
    }

    @Override
    public boolean canPushEntity(Entity e, MoveType moveType) {
        return this != e && collisionMap.canPush(e);
    }

    @Override
    public boolean canOverlapWithEntity(Entity e) {
        return this == e || collisionMap.canOverlap(e);
    }

    @Override
    public boolean canOverlapOnCell(Cell cell) {
        for (Entity entity: cell.getEntities()) {
            if (!canOverlapWithEntity(entity)) return false;
        }
        return true;
    }

    @Override
    public boolean canPushCell(Cell cell, MoveType moveType) {
        for (Entity entity: cell.getEntities()) {
            if (!canPushEntity(entity, moveType)) return false;
        }
        return true;
    }

    @Override
    public void setEntityMover(EntityMover mover) {
        this.entityMover = mover;
    }

    @Override
    public Cell getCell() {
        return cell;
    }

    @Override
    public float getX() { return x; }

    @Override
    public float getY() { return y; }

    public boolean entityMoverActive() {
        return entityMover != null;
    }
}
