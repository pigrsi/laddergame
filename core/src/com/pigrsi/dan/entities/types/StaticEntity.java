package com.pigrsi.dan.entities.types;

import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.gameworld.Grid;

import java.util.List;

public abstract class StaticEntity extends BasicEntity {
    private float x, y;
    private Cell cell;

    public StaticEntity(Grid grid) {
        super(grid);
    }

    @Override
    public void update(float delta) { }

    @Override
    public void tryMove(Direction direction) { }

    @Override
    public void move(int rowOffset, int columnOffset, MoveType moveType, Cell newCell) { }

    @Override
    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void setCell(Cell newCell, boolean resetPosition) {
        setPosition(newCell.getWorldX(), newCell.getWorldY());
        cell = newCell;
    }

    @Override
    public void onEntityMove() { super.onEntityMove(); }

    @Override
    public float getX() {
        return x;
    }

    @Override
    public float getY() {
        return y;
    }

    @Override
    public Cell getCell() {
        return cell;
    }

    @Override
    public boolean canStandOnEntity(Entity e) {
        return false;
    }

    @Override
    public boolean canPushEntity(Entity e, MoveType moveType) {
        return false;
    }

    @Override
    public boolean canClimbEntity(Entity e) {
        return false;
    }

    @Override
    public boolean canOverlapWithEntity(Entity e) {
        return false;
    }

    @Override
    public boolean isStill() {return true;}
}
