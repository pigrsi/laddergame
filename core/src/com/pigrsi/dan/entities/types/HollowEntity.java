package com.pigrsi.dan.entities.types;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pigrsi.dan.animators.Animator;
import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.gameworld.Grid;

import java.util.List;
import java.util.Set;

public abstract class HollowEntity implements Entity {
    Grid grid;
    Animator animator;
    private String name = getClass().getSimpleName();
    private Cell cell;
    private float x, y;

    public HollowEntity(Grid grid) {
        this.grid = grid;
        createAnimator();
    }

    @Override
    public void render(SpriteBatch batch) {
        if (cell != null) animator.render(batch);
    }

    @Override
    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void setCell(Cell newCell, boolean resetPosition) {
        if (newCell != null) setPosition(newCell.getWorldX(), newCell.getWorldY());
        cell = newCell;
    }

    @Override
    public Cell getCell() {
        return cell;
    }

    @Override
    public boolean isStill() {
        return true;
    }

    @Override
    public boolean isAlive() {
        return true;
    }

    @Override
    public float getX() {
        return x;
    }

    @Override
    public float getY() {
        return y;
    }

    @Override
    public void dispose() {
        animator.dispose();
    }

    @Override
    public void setName(String name) { this.name = name; }

    @Override
    public String getName() { return name; }

    @Override
    public void update(float delta) { }

    @Override
    public void postUpdate(float delta) { }

    @Override
    public void tryMove(Direction direction) { }

    @Override
    public void onEntityMove() { }

    @Override
    public void onGrabbedByEntity(Entity grabber) { }

    @Override
    public void onDroppedByEntity() { }

    @Override
    public void move(int rowOffset, int columnOffset, MoveType moveType, Cell newCell) { }

    @Override
    public void mergeGroups(Entity entity) { }

    @Override
    public void setGroup(Set<Entity> group) { }

    @Override
    public void onEndFall(FallStopper fallStopper) { }

    @Override
    public void setPreviousFallStopper(FallStopper fallStopper) { }

    @Override
    public void setAlive(boolean alive) { }

    @Override
    public void createAnimator() { }

    @Override
    public Set<Entity> getGroup() { return null; }

    @Override
    public FallStopper getPreviousFallStopper() { return null; }

    @Override
    public Entity getGrabbedEntity() { return null; }

    @Override
    public Entity getEntityGrabbingThis() { return null; }

    @Override
    public boolean canStandOnCell(Cell cell) { return false; }

    @Override
    public boolean canClimb() { return false; }

    @Override
    public boolean checkForAutomove() {
        return false;
    }
}
