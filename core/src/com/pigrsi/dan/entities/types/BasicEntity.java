package com.pigrsi.dan.entities.types;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pigrsi.dan.entityhelpers.EntityMover;
import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.gameworld.Grid;
import com.pigrsi.dan.animators.Animator;

import java.util.HashSet;
import java.util.Set;

public abstract class BasicEntity implements Entity {
    protected Set<Entity> group = new HashSet<>();
    protected Grid grid;
    protected FallStopper previousFallStopper;
    protected Animator animator;
    private boolean alive = true;
    private String name = getClass().getSimpleName();

    public BasicEntity(Grid grid) {
        this.grid = grid;
        group.add(this);
        createAnimator();
    }

    @Override
    public void mergeGroups(Entity entity) {
        Gdx.app.log(getClass().getSimpleName(), "Merging groups containing " + getClass() + " and " + entity.getClass());
        group.addAll(entity.getGroup());
        for (Entity groupEntity: group) {
            groupEntity.setGroup(group);
        }
        for (Entity groupEntity: group) {
            groupEntity.onGroupChanged();
        }
    }

    @Override
    public void onEndFall(FallStopper fallStopper) {
        setPreviousFallStopper(fallStopper);
    }

    @Override
    public void onGrabbedByEntity(Entity grabber) { }

    @Override
    public void onDroppedByEntity() {}

    @Override
    public Entity getGrabbedEntity() { return null; }

    @Override
    public Entity getEntityGrabbingThis() { return null; }

    @Override
    public FallStopper getPreviousFallStopper() {
        return previousFallStopper;
    }

    @Override
    public void setPreviousFallStopper(FallStopper fallStopper) {
        this.previousFallStopper = fallStopper;
//        Gdx.app.log(getClass().getSimpleName(), "Setting previousFallStopper to " + fallStopper);
    }

    @Override
    public void postUpdate(float delta) {}

    @Override
    public void render(SpriteBatch batch) {
        if (getCell() != null) animator.render(batch);
    }

    @Override
    public void onEntityMove() {
        Cell cell = getCell();
        if (cell == null || cell.isDeathCell())
            setAlive(false);
    }

    @Override
    public void onGroupChanged() {
        if (getEntityGrabbingThis() != null) getEntityGrabbingThis().onGroupChanged();
    }

    @Override
    public void dropEntity() {}

    @Override
    public void beforeStartFall() {}

    @Override
    public void setGroup(Set<Entity> group) {
        this.group = group;
    }

    @Override
    public Set<Entity> getGroup() {
        return group;
    }

    @Override
    public boolean canClimb() { return false; }

    @Override
    public boolean canStandOnCell(Cell cell) { return false; }

    @Override
    public boolean checkForAutomove() { return false; }

    @Override
    public boolean isAlive() { return alive; }

    @Override
    public void setAlive(boolean alive) { this.alive = alive; }
    @Override
    public void setName(String name) { this.name = name; }

    @Override
    public String getName() { return name; }

    @Override
    public boolean canClimbEntity(Entity e) { return false; }

    @Override
    public boolean canOverlapOnCell(Cell cell) { return false; }

    @Override
    public boolean canPushCell(Cell cell, MoveType moveType) { return false; }

    @Override
    public boolean canClimbOnCell(Cell cell) { return false; }

    @Override
    public void setEntityMover(EntityMover mover) {}

    @Override
    public Animator getAnimator() {return animator;}

    @Override
    public void dispose() {
        animator.dispose();
    }
}
