package com.pigrsi.dan.rendering;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class GridRenderer {

    private final int numRows;
    private final int numColumns;
    private final int gridWidth;
    private final int gridHeight;
    private final int cellSize;
    private final boolean borderOnly;
    private ShapeRenderer shapeRenderer;

    public GridRenderer(int numRows, int numColumns, int cellSize, ShapeRenderer shapeRenderer, boolean borderOnly) {
        this.numRows = numRows;
        this.numColumns = numColumns;
        this.cellSize = cellSize;
        gridWidth = numColumns * cellSize;
        gridHeight = numRows * cellSize;
        this.shapeRenderer = shapeRenderer;
        this.borderOnly = borderOnly;
    }

    public void render(Camera camera) {
        Gdx.gl.glLineWidth(1);
        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.DARK_GRAY);

        if (borderOnly) {
            shapeRenderer.line(0, 0, gridWidth, 0);
            shapeRenderer.line(gridWidth, 0, gridWidth, gridHeight);
            shapeRenderer.line(gridWidth, gridHeight, 0, gridHeight);
            shapeRenderer.line(0, gridHeight, 0, 0);
        } else {
            for (int currentRow = 0; currentRow <= numRows; ++currentRow)
                shapeRenderer.line(0, currentRow * cellSize, numColumns * cellSize, currentRow * cellSize);
            for (int currentColumn = 0; currentColumn <= numColumns; ++currentColumn)
                shapeRenderer.line(currentColumn * cellSize, 0, currentColumn * cellSize, numRows * cellSize);
        }
        shapeRenderer.end();
    }

}
