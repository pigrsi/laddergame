package com.pigrsi.dan.rendering;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.pigrsi.dan.entities.block.Balloon;
import com.pigrsi.dan.entities.ladder.Ladder;
import com.pigrsi.dan.entities.types.Mergable;
import com.pigrsi.dan.gameworld.World;
import com.pigrsi.dan.entities.types.Entity;

import static com.pigrsi.dan.gameworld.World.CELL_SIZE;

public class DebugRenderer {

    private final boolean SHOW_GROUP_LINKS = false;
    private final boolean RENDER_GRID = true;
    private final boolean GRID_BORDER_ONLY = true;

    private ShapeRenderer shapeRenderer;
    private GridRenderer gridRenderer;
    private World world;

    public DebugRenderer(World world) {
        this.world = world;
        shapeRenderer = new ShapeRenderer();
        gridRenderer = new GridRenderer(world.grid.getNumRows(), world.grid.getNumColumns(), CELL_SIZE, shapeRenderer, GRID_BORDER_ONLY);
    }

    public void frontRender(Camera camera) {
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        for (Entity entity: world.entityManager.movableEntities) {
            if (entity.getEntityGrabbingThis() != null) {
                shapeRenderer.setColor(1, 0.5f, 0, 0.5f);
                shapeRenderer.rect(entity.getX(), entity.getY(), CELL_SIZE, CELL_SIZE);
            }
        }

        if (SHOW_GROUP_LINKS) showGroupLinks();

        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    public void backRender(Camera camera) {
        if (RENDER_GRID) gridRenderer.render(camera);
    }

    private void showGroupLinks() {
        int halfWidth = CELL_SIZE/2;
        shapeRenderer.setColor(Color.BLACK);
        for (Entity entity: world.entityManager.getAllEntities()) {
            if (entity instanceof Mergable) {
                if (entity instanceof Ladder) {
                    shapeRenderer.setColor(Color.PINK);
                } else if (entity instanceof Balloon) {
                    shapeRenderer.setColor(Color.YELLOW);
                }
                for (Entity groupEntity: entity.getGroup()) {
                    shapeRenderer.rectLine(new Vector2(entity.getX() + halfWidth, entity.getY() + halfWidth), new Vector2(groupEntity.getX() + halfWidth, groupEntity.getY() + halfWidth), 4);
                }
            }
        }
    }

}
