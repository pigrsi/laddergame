package com.pigrsi.dan;

import com.pigrsi.dan.entities.types.Entity;

public interface GridStateListener {
    void onGridStateChange(Entity instigator);
}
