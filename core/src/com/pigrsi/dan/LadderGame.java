package com.pigrsi.dan;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.pigrsi.dan.gameworld.World;
import com.pigrsi.dan.input.InputReceiver;

import java.util.Arrays;
import java.util.List;

public class LadderGame extends ApplicationAdapter {
	private World world;
	private InputReceiver inputReceiver;

	@Override
	public void create () {
		createInput();
		world = new World();
		world.create(inputReceiver);
	}

	private void createInput() {
		inputReceiver = new InputReceiver();
		Gdx.input.setInputProcessor(inputReceiver);
	}

	@Override
	public void render () {
		List<Integer> keys = Arrays.asList(
				Input.Keys.UP, Input.Keys.DOWN, Input.Keys.LEFT, Input.Keys.RIGHT
		);
		for (Integer key : keys) {
			if (Gdx.input.isKeyPressed(key)) {
				inputReceiver.keyDown(key);
				break;
			}
		}

	    float delta = Gdx.graphics.getDeltaTime();
		Gdx.gl.glClearColor(0.58f, 1, 0.55f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		world.render(delta);
	}

	@Override
	public void resize (int width, int height) {
		world.resize(width, height);
	}
	
//	@Override
//	public void inputDown() {
//		world.inputDown();
//	}
//
//	@Override
//	public void inputUp() { world.inputUp(); }
//
//	@Override
//	public void inputLeft() {
//		world.inputLeft();
//	}
//
//	@Override
//	public void inputRight() { world.inputRight(); }
//
//	@Override
//	public void inputGrab() { world.inputGrab(); }
//
//	@Override
//	public void inputUndo() { world.undoFromPlayer(); }
//
//	@Override
//	public void inputSwitchMode() { world.switchMoveMode(); }
//
//	@Override
//	public void inputRotateLeft() { world.rotateLeft(); }
//
//	@Override
//	public void inputRotateRight() { world.rotateRight(); }

	@Override
	public void dispose () {
		world.dispose();
	}
}
