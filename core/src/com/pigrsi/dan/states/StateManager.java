package com.pigrsi.dan.states;

import com.badlogic.gdx.Gdx;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Long.MIN_VALUE;

public class StateManager {
    private List<State> states;
    private long nextStateId = MIN_VALUE;

    public StateManager() {
        states = new ArrayList<>();
    }

    public void addState(State state) {
        if (shouldAddNewState(state)) {
            states.add(state);
            Gdx.app.log(getClass().getSimpleName(), "State added to undo stack");
        }
    }

    private boolean shouldAddNewState(State newState) {
        return states.isEmpty() || !State.statesAreEquivalent(getMostRecentState(), newState);
    }

    private State getMostRecentState() {
        return states.isEmpty() ? null : states.get(states.size() - 1);
    }

    public long incrementStateId() { return ++nextStateId; }

    public long getNextStateId() { return nextStateId; }

    public void undo() {
        boolean stateHasChanged = false;
        while (!stateHasChanged) {
            if (states.isEmpty()) { Gdx.app.log(getClass().getSimpleName(), "States list empty"); return; }
            stateHasChanged = states.remove(states.size() - 1).apply();
            Gdx.app.log(getClass().getSimpleName(), "Move undone");
        }
    }
}
