package com.pigrsi.dan.states;

import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.entities.block.PowerBlock;
import com.pigrsi.dan.gameworld.Cell;

public class PowerBlockState extends EntityState {
    private boolean railsDeployed;

    public PowerBlockState(Entity entity, Cell cell, boolean alive, boolean railsDeployed) {
        super(entity, cell, alive);
        this.railsDeployed = railsDeployed;
    }

    public boolean changeBetweenStatesIsNotable(EntityState entityState1, EntityState entityState2) {
        PowerBlockState powerBlockState1 = (PowerBlockState) entityState1;
        PowerBlockState powerBlockState2 = (PowerBlockState) entityState2;
        return super.changeBetweenStatesIsNotable(entityState1, entityState2) || powerBlockState1.railsDeployed != powerBlockState2.railsDeployed;
    }

    public boolean apply() {
        boolean stateChanged = super.apply();
        PowerBlock powerBlock = (PowerBlock) entity;
        if (railsDeployed != powerBlock.railsAreDeployed()) {
            powerBlock.setRailsDeployed(railsDeployed);
            stateChanged = true;
        }
        return stateChanged;
    }


}
