package com.pigrsi.dan.states;

import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.entities.types.RotaryEntity;
import com.pigrsi.dan.gameworld.Cell;

public class RotaryState extends EntityState {
    private Entity.Orientation orientation;

    public RotaryState(Entity entity, Cell cell, boolean alive, Entity.Orientation orientation) {
        super(entity, cell, alive);
        this.orientation = orientation;
    }

    public boolean changeBetweenStatesIsNotable(EntityState entityState1, EntityState entityState2) {
        RotaryState rotaryState1 = (RotaryState) entityState1;
        RotaryState rotaryState2 = (RotaryState) entityState2;
        return super.changeBetweenStatesIsNotable(entityState1, entityState2) || rotaryState1.orientation != rotaryState2.orientation;
    }

    public boolean apply() {
        boolean stateChanged = super.apply();
        RotaryEntity rotaryEntity = (RotaryEntity) entity;
        if (orientation != rotaryEntity.getOrientation()) {
            rotaryEntity.setOrientation(orientation);
            stateChanged = true;
        }
        return stateChanged;
    }


}
