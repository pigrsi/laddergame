package com.pigrsi.dan.states;

import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.entities.misc.Player;

public class PlayerState extends EntityState {
    private Entity grabbedEntity;
    private Player.MoveMode moveMode;
    private Entity.FallStopper previousFallStopper;

    public PlayerState(Player player, Cell cell, Entity grabbedEntity, Player.MoveMode moveMode, Entity.FallStopper previousFallStopper) {
        super(player, cell);
        this.grabbedEntity = grabbedEntity;
        this.moveMode = moveMode;
        this.previousFallStopper = previousFallStopper;
    }

    public boolean changeBetweenStatesIsNotable(EntityState entityState1, EntityState entityState2) {
        PlayerState playerState1 = (PlayerState) entityState1;
        PlayerState playerState2 = (PlayerState) entityState2;
        if (playerState1.grabbedEntity != playerState2.grabbedEntity) return true;
        if (playerState1.moveMode != playerState2.moveMode) return true;
        if (playerState1.alive != playerState2.alive) return true;
        if (playerState1.cell != playerState2.cell) return true;
        return false;
    }

    // Returns whether or not the entity's state was changed.
    public boolean apply() {
        boolean stateChanged = super.apply();
        Player player = (Player) entity;
        if (player.getMoveMode() != moveMode) {
            player.setMoveMode(moveMode);
            stateChanged = true;
        }
        if (player.getGrabbedEntity() != grabbedEntity) {
            if (player.isHoldingSomething() && grabbedEntity == null) {
                player.dropEntity();
            } else {
                player.grabEntity(grabbedEntity);
            }
            stateChanged = true;
        }
        if (player.getPreviousFallStopper() != previousFallStopper) {
            player.setPreviousFallStopper(previousFallStopper);
            stateChanged = true;
        }
        return stateChanged;
    }

    public Player.MoveMode getMoveMode() {return moveMode;}
}
