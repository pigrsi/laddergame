package com.pigrsi.dan.states;

import com.pigrsi.dan.entities.block.PowerBlock;
import com.pigrsi.dan.entities.types.RotaryEntity;
import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.entities.misc.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class State {
    private Map<Entity, EntityState> entityStates;
    private long id;

    public State(List<Entity> entities, long id) {
        extractEntityStates(entities);
        this.id = id;
    }

    private void extractEntityStates(List<Entity> entities) {
        entityStates = new HashMap<>();
        for (Entity entity: entities) {
            if (entity instanceof RotaryEntity) {
                RotaryEntity rotary = (RotaryEntity) entity;
                entityStates.put(entity, (new RotaryState(rotary, entity.getCell(), rotary.isAlive(), rotary.getOrientation())));
            } else if (entity.getClass() == Player.class) {
                Player player = (Player) entity;
                entityStates.put(entity, (new PlayerState(player, entity.getCell(), player.getGrabbedEntity(), player.getMoveMode(), player.getPreviousFallStopper())));
            } else if (entity instanceof PowerBlock) {
                PowerBlock powerBlock = (PowerBlock) entity;
                entityStates.put(entity, (new PowerBlockState(powerBlock, powerBlock.getCell(), powerBlock.isAlive(), powerBlock.railsAreDeployed())));
            } else {
                entityStates.put(entity, (new EntityState(entity, entity.getCell())));
            }
        }
    }

    // Returns whether or not the state was changed.
    public boolean apply() {
        boolean stateHasChanged = false;
        for (EntityState entityState: entityStates.values()) {
            if (entityState.apply()) stateHasChanged = true;
        }
        return stateHasChanged;
    }

    public static boolean statesAreEquivalent(State state1, State state2) {
        if (state1.id == state2.id) return true;
        for (EntityState entityState1: state1.entityStates.values()) {
            EntityState entityState2 = state2.entityStates.get(entityState1.getEntity());
            if (entityState2 == null) return false;
            if (entityState1.changeBetweenStatesIsNotable(entityState1, entityState2)) return false;
        }
        return true;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        String result = "Entity states:\n";
        if (entityStates.isEmpty()) result += "None";
        for (EntityState e : entityStates.values()) {
            Cell cell = e.getCell();
            result += String.format("Type: %s, row: %d, column: %d", e.getClass(), cell.getRow(), cell.getColumn()) + "\n";
        }
        return result;
    }
}
