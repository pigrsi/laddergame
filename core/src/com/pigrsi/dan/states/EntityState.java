package com.pigrsi.dan.states;

import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.entities.types.Entity;

import java.util.HashSet;
import java.util.Set;

public class EntityState {
    Entity entity;
    Cell cell;
    Set<Entity> group;
    boolean alive;

    public EntityState(Entity entity, Cell cell, boolean alive) {
        this.entity = entity;
        this.cell = cell;
        this.alive = alive;
        group = new HashSet<>();
        Set<Entity> entityGroup = entity.getGroup();
        if (entityGroup != null) group.addAll(entity.getGroup());
    }

    public EntityState(Entity entity, Cell cell) {
        this(entity, cell, true);
    }

    public boolean changeBetweenStatesIsNotable(EntityState entityState1, EntityState entityState2) {
        return entityState1.getCell() != entityState2.getCell() ||
                entityState1.alive != entityState2.alive;
    }

    // Returns whether or not the entity's state was changed.
    public boolean apply() {
        if (entity.getCell() == cell && groupsAreIdentical()) return false;
        entity.setCell(cell, true);
        entity.setGroup(group);
        entity.setAlive(alive);
        return true;
    }

    private boolean groupsAreIdentical() {
        if (entity.getGroup() == null) return true;
        if (group.size() != entity.getGroup().size()) return false;
        for (Entity groupEntity: group)
            if (!entity.getGroup().contains(groupEntity)) return false;
        for (Entity groupEntity: entity.getGroup())
            if (!group.contains(groupEntity)) return false;
        return true;
    }

    public Entity getEntity() {
        return entity;
    }

    public Cell getCell() {
        return cell;
    }
}
