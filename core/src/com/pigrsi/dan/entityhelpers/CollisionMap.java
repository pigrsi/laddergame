package com.pigrsi.dan.entityhelpers;

import com.pigrsi.dan.entities.types.Entity;

import java.util.HashMap;
import java.util.Map;

public class CollisionMap {
    private Map<Integer, Boolean[]> collisionMap = new HashMap<>();

    public void addCollision(boolean stand, boolean push, boolean overlap, int... id) {
        for (int i = 0; i < id.length; ++i) collisionMap.put(id[i], new Boolean[] {stand, push, overlap});
    }

    public boolean canStandOn(Entity e) { return collisionMap.containsKey(e.getCollisionID()) ?  collisionMap.get(e.getCollisionID())[0] : false; }

    public boolean canPush(Entity e) { return collisionMap.containsKey(e.getCollisionID()) ?  collisionMap.get(e.getCollisionID())[1] : false; }

    public boolean canOverlap(Entity e) { return collisionMap.containsKey(e.getCollisionID()) ?  collisionMap.get(e.getCollisionID())[2] : true; }
}
