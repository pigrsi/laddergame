package com.pigrsi.dan.entityhelpers;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.pigrsi.dan.entities.types.Entity;

public class OrbitMover implements EntityMover {

    private Interpolation interp = Interpolation.linear;
    private boolean done = false;
    private Entity entity;
    private float lifetime = 0.2f;
    private float elapsed = 0;
    private float progress = 0;

    private Vector2 start;
    private Vector2 about;
    private float degrees;

    public OrbitMover(Entity entity, float aboutX, float aboutY, float degrees) {
        this.entity = entity;
        this.degrees = degrees;
        start = new Vector2(entity.getX(), entity.getY());
        about = new Vector2(aboutX, aboutY);
    }

    @Override
    public void update(float delta) {
        progress = Math.min(1f, elapsed / lifetime);
        float t = interp.apply(progress);
        float amount = degrees * t;
        Vector2 newPos = new Vector2(start);
        newPos.rotateAround(about, amount);
        entity.setPosition(newPos.x, newPos.y);
        done = elapsed >= lifetime;
        elapsed += delta;
    }

    public float getProgress() {
        return progress;
    }

    public float getDegreesToOrbit() {
        return degrees;
    }

    @Override
    public void setInterpolation(Interpolation interp) { this.interp = interp; }

    @Override
    public void setLifetime(float lifetime) { this.lifetime = lifetime; }

    @Override
    public boolean isDone() { return done; }

}
