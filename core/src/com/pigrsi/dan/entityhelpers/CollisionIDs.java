package com.pigrsi.dan.entityhelpers;

public class CollisionIDs {
    public static int PLAYER = 0;
    public static int BLOCK = 1;
    public static int POWERBLOCK = 2;
    public static int THRUBLOCK = 3;
    public static int CRAPLADDER = 4;
    public static int LADDER = 5;
    public static int POWERLADDER = 6;
    public static int RAIL = 7;
    public static int SPIKE = 8;
    public static int BALLOON = 9;
    public static int STICKY_BLOCK = 10;
}
