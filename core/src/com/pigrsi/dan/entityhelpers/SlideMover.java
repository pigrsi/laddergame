package com.pigrsi.dan.entityhelpers;

import com.badlogic.gdx.math.Interpolation;
import com.pigrsi.dan.entities.types.Entity;

public class SlideMover extends BasicMover {
    public SlideMover(Entity entity, float destinationX, float destinationY) {
        super(entity, destinationX, destinationY);
        setInterpolation(Interpolation.circleOut);
        setLifetime(0.1f);
    }
}
