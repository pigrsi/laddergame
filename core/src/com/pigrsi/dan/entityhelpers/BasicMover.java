package com.pigrsi.dan.entityhelpers;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.pigrsi.dan.entities.types.Entity;

public abstract class BasicMover implements EntityMover {

    private Interpolation interp = Interpolation.linear;
    private boolean done = false;
    private Entity entity;
    private float lifetime = 0.1f;
    private float elapsed = 0;

    private Vector2 start;
    private Vector2 distance;

    public BasicMover(Entity entity, float destinationX, float destinationY) {
        this.entity = entity;
        start = new Vector2(entity.getX(), entity.getY());
        distance = new Vector2(destinationX - start.x, destinationY - start.y);
    }

    @Override
    public void update(float delta) {
        float progress = Math.min(1f, elapsed / lifetime);
        float t = interp.apply(progress);
        entity.setPosition(start.x + distance.x * t, start.y + distance.y * t);
        done = elapsed >= lifetime;
        elapsed += delta;
    }

    @Override
    public void setInterpolation(Interpolation interp) { this.interp = interp; }

    @Override
    public void setLifetime(float lifetime) { this.lifetime = lifetime; }

    @Override
    public boolean isDone() { return done; }
}
