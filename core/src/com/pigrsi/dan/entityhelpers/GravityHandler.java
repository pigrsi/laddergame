package com.pigrsi.dan.entityhelpers;

import com.pigrsi.dan.entities.types.FallingEntity;
import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.gameworld.Grid;
import com.pigrsi.dan.gameworld.World;
import com.pigrsi.dan.entities.types.Entity;

import java.util.HashSet;
import java.util.Set;

public class GravityHandler {

    private final FallingEntity entity;
    private final Grid grid;
    private boolean satisfied = true;
    private boolean bufferFrame = false;
    private boolean enabled = true;

    public GravityHandler(FallingEntity entity, Grid grid) {
        this.entity = entity;
        this.grid = grid;
    }

    public void startFall() {
        if (!enabled) return;
        satisfied = false;
        bufferFrame = true;
    }

    public void update() {
        Cell entityCell = entity.getCell();
        Cell landingCell = null;
        Cell currentCell = grid.getCell(entityCell.getRow() - 1, entityCell.getColumn());
        while (true) {
            if (entity.canStandOnCell(currentCell)) {
                landingCell = currentCell;
                break;
            }
            if (currentCell.getRow() - 1 < 0) break;
            currentCell = grid.getCell(currentCell.getRow() - 1, currentCell.getColumn());
        }
        // If landing cell is cell below climbable, land on the landing cell and not the climbable
        if (landingCell == null || landingCell.getRow() != entityCell.getRow() - 1) {
            if (checkLandOnClimbable(entityCell)) return;
        }
        Cell grabbedLandingCell = checkLandOnGrabbed(entityCell);

        Entity.FallStopper fallStopper = Entity.FallStopper.FLOOR;
        if (grabbedLandingCell != null) {
            if (landingCell == null || grabbedLandingCell.getRow() > landingCell.getRow()) {
                landingCell = grabbedLandingCell;
                fallStopper = Entity.FallStopper.GRAB;
            }
        } else {
            if (EntityUtil.landingShouldCountAsClimb(grid, landingCell, entity)) fallStopper = Entity.FallStopper.CLIMB;
        }

        fall(landingCell, fallStopper);
    }

    private Cell checkLandOnGrabbed(Cell entityCell) {
        Entity grabbedEntity = entity.getGrabbedEntity();
        // If my entity is holding another
        if (grabbedEntity != null && grabbedEntity.isStill()) {
            for (Entity grabbed: grabbedEntity.getGroup()) {
                if (Cell.cellsAreTouching(grabbed.getCell(), entity.getCell()))
                    return grid.getCell(entityCell.getRow() - 1, entityCell.getColumn());
            }
        } else { // If some entity is holding mine, check if any part of my group is being held
            Entity grabbedBy = null;
            for (Entity groupEntity: entity.getGroup()) {
                if (groupEntity.getEntityGrabbingThis() != null) {
                    grabbedBy = groupEntity.getEntityGrabbingThis();
                    break;
                }
            }
            if (grabbedBy == null || !grabbedBy.isStill()) return null;

            if (Cell.cellsAreTouching(grabbedBy.getCell(), entity.getCell()))
                return grid.getCell(entityCell.getRow() - 1, entityCell.getColumn());
        }

        return null;
    }

    private boolean checkLandOnClimbable(Cell entityCell) {
        boolean canLand = false;
        if (entity.canClimb()) {
            int row = entityCell.getRow(), column = entityCell.getColumn();

            Cell[] cellsToCheck = new Cell[] {entityCell,
                    grid.getCell(row, column-1),
                    grid.getCell(row, column+1),
                    grid.getCell(row+1, column)};

            for (Cell cell: cellsToCheck) {
                for (Entity potentialClimbable: cell.getEntities()) {
                    if (entity.canClimbEntity(potentialClimbable)) {
                        canLand = true;
                        break;
                    }
                }
            }

            if (canLand) endFall(entityCell, Entity.FallStopper.CLIMB);
        }
        return canLand;
    }

    private void fall(Cell landingCell, Entity.FallStopper fallStopper) {
        if (satisfied) return;
        setEntityCell();

        if (!satisfied && !bufferFrame) applyGravity();
        bufferFrame = false;

        if (landingCell != null && entity.getY() <= landingCell.getWorldY() + World.CELL_SIZE) {
            endFall(grid.getCell(landingCell.getRow() + 1, landingCell.getColumn()), fallStopper);
        }

        if (entity.getY() < -World.CELL_SIZE) entity.setAlive(false);
    }

    private void applyGravity() {
        entity.setPosition(entity.getX(), entity.getY() - 12);
    }

    public void endFall(Cell snapCell, Entity.FallStopper fallStopper) {
        satisfied = true;
        entity.setCell(snapCell, true);
        entity.onEndFall(fallStopper);
    }

    private void setEntityCell() {
        Cell currentCell = entity.getCell();
        if (entity.getY() + World.CELL_SIZE/2 <= currentCell.getWorldY()) {
            Cell newCell = grid.getCell(currentCell.getRow() - 1, currentCell.getColumn());
            if (newCell != null) {
                entity.setCell(newCell, false);
                entity.onFellIntoCell(newCell, currentCell);
                if (entity.getGrabbedEntity() != null && entity.getGrabbedEntity().isStill()) entity.dropEntity();
            }
        }
    }

    public void checkGroupIsSatisfied() {
        // Copy into new set to avoid concurrent modification
        Set<Entity> group = new HashSet<>(entity.getGroup());
        for (Entity linkedEntity : group)
            if (linkedEntity.isStill()) {
                endFall(entity.getCell(), Entity.FallStopper.GROUP);
            }
    }

    public void disable() { this.enabled = false; }

    public void enable() { this.enabled = true; }

    public boolean isSatisfied() {
        return satisfied;
    }

}