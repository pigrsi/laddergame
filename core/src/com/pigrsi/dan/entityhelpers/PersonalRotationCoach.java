package com.pigrsi.dan.entityhelpers;

import com.badlogic.gdx.Gdx;
import com.pigrsi.dan.animators.Animator;
import com.pigrsi.dan.animators.RotaryAnimator;
import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.gameworld.Grid;

public class PersonalRotationCoach {
    public Entity client;
    public Entity.Orientation orientation = Entity.Orientation.UP;
    public Entity.RotationDirection previousRotation;
    public Cell previousRotationOriginCell;

    public PersonalRotationCoach(Entity client) {this.client = client;}

    public void rotate(Cell about, Entity.RotationDirection rotationDirection, Grid grid) {
        if (about == null) {
            Gdx.app.log(getClass().getSimpleName(), "Cell to rotate about is null.");
            return;
        }
        Cell myCell = client.getCell();
        int rowOffset = myCell.getRow() - about.getRow();
        int columnOffset = myCell.getColumn() - about.getColumn();
        Cell newCell;
        if (rotationDirection == Entity.RotationDirection.CLOCKWISE)
            newCell = grid.getCell(about.getRow() - columnOffset, about.getColumn() + rowOffset);
        else
            newCell = grid.getCell(about.getRow() + columnOffset, about.getColumn() - rowOffset);

        OrbitMover mover = new OrbitMover(client, about.getWorldX(), about.getWorldY(), rotationDirection == Entity.RotationDirection.CLOCKWISE ? -90 : 90);
        client.setEntityMover(mover);
        Animator animator = client.getAnimator();
        if (animator instanceof RotaryAnimator) ((RotaryAnimator) client.getAnimator()).rotateWithOrbit(mover);
        client.setCell(newCell, false);

        previousRotation = rotationDirection;
        previousRotationOriginCell = about;
        orientation = EntityUtil.getRotatedOrientation(orientation, rotationDirection);
    }

    public void reverseLastRotation(Grid grid) {
        if (previousRotationOriginCell == null) { Gdx.app.log(getClass().getSimpleName(), "Cannot reverse rotation, prev rotation cell is null."); return; }
        if (previousRotation == null) { Gdx.app.log(getClass().getSimpleName(), "Cannot reverse rotation, prev rotation direction is null."); return; }
        rotate(previousRotationOriginCell, previousRotation == Entity.RotationDirection.CLOCKWISE ? Entity.RotationDirection.ANTICLOCKWISE : Entity.RotationDirection.CLOCKWISE, grid);
    }
}
