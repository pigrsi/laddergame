package com.pigrsi.dan.entityhelpers;

import com.pigrsi.dan.entities.ladder.Ladder;
import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.gameworld.Cell;
import com.pigrsi.dan.gameworld.Grid;

import java.util.HashMap;
import java.util.Map;

public class EntityUtil {
    private static Cell nullCell = new Cell(-1, -1);

    public static Entity.Orientation getRotatedOrientation(Entity.Orientation orientation, Entity.RotationDirection rotationDirection) {
        switch (orientation) {
            case UP:
                orientation = rotationDirection == Entity.RotationDirection.CLOCKWISE ? Entity.Orientation.RIGHT : Entity.Orientation.LEFT;
                break;
            case DOWN:
                orientation = rotationDirection == Entity.RotationDirection.CLOCKWISE ? Entity.Orientation.LEFT : Entity.Orientation.RIGHT;
                break;
            case LEFT:
                orientation = rotationDirection == Entity.RotationDirection.CLOCKWISE ? Entity.Orientation.UP : Entity.Orientation.DOWN;
                break;
            case RIGHT:
                orientation = rotationDirection == Entity.RotationDirection.CLOCKWISE ? Entity.Orientation.DOWN : Entity.Orientation.UP;
                break;
        }
        return orientation;
    }

    public static boolean groupsAreTouching(Entity entity1, Entity entity2) {
        for (Entity e1: entity1.getGroup()) {
            for (Entity e2: entity2.getGroup()) {
                if (Cell.cellsAreTouching(e1.getCell(), e2.getCell())) return true;
            }
        }
        return false;
    }

    public static boolean playerIsTouchingALadderInGroup(Entity player, Entity groupEntity) {
        for (Entity e2: groupEntity.getGroup()) {
            if (!(e2 instanceof Ladder)) continue;
            if (Cell.cellsAreTouching(player.getCell(), e2.getCell())) return true;
        }
        return false;
    }

    public static boolean groupIsAboveGroup(Entity entity1, Entity entity2) {
        for (Entity e1: entity1.getGroup()) {
            for (Entity e2: entity2.getGroup()) {
                if (e1.getCell().getRow() <= e2.getCell().getRow()) return false;
            }
        }
        return true;
    }

    public static boolean playerIsAboveAllLaddersInGroup(Entity player, Entity groupEntity) {
        for (Entity e2: groupEntity.getGroup()) {
            if (!(e2 instanceof Ladder)) continue;
            if (player.getCell().getRow() <= e2.getCell().getRow()) return false;
        }
        return true;
    }

    public static boolean landingShouldCountAsClimb(Grid grid, Cell landingCell, Entity entity) {
        /* (when it could also be a FLOOR landing) */
        if (landingCell == null) return true;
        if (entity.canStandOnCell(landingCell)) return false;
        Entity landingClimbable = getStillClimbableEntityInCell(landingCell, entity);
        Entity overlappingClimbable = getStillClimbableEntityInCell(grid.getCell(landingCell.getRow() + 1, landingCell.getColumn()), entity);
        if (landingClimbable != null) {
            if (overlappingClimbable != null) {
                // CLIMBABLE: Both not null, in same group
                // FLOOR: Both not null, not same group
                if (landingClimbable.getGroup().contains(overlappingClimbable)){
                    return true;
                }
                else return false;
            } else {
                // FLOOR: Landing cell not null, overlapping cell null
                return false;
            }
        } else {
            // CLIMBABLE: Landing cell null, overlapping not null
            if (overlappingClimbable != null) return true;
        }
        return false;
    }

    private static Entity getStillClimbableEntityInCell(Cell cell, Entity entity) {
        for (Entity potentialClimbable: cell.getEntities()) if (potentialClimbable.isStill() && entity.canClimbEntity(potentialClimbable)) return potentialClimbable;
        return null;
    }

    public static boolean groupIsStandingOnOnlyMe(Entity entityInGroup, Entity me, Grid grid) {
        boolean standingOnMe = true;
        Cell prevCell = me.getCell();
        me.setCell(nullCell, false);
        for (Entity groupEntity: entityInGroup.getGroup()) {
            Cell cell = groupEntity.getCell();
            if (groupEntity.canStandOnCell(grid.getCell(cell.getRow() - 1, cell.getColumn()))) {
                standingOnMe = false;
                break;
            }
        }
        me.setCell(prevCell, false);
        return standingOnMe;
    }

    public static Cell[] getAdjacentCells(Cell startingCell, Grid grid) {
        return new Cell[] {
            grid.getCell(startingCell.getRow() - 1, startingCell.getColumn()),
            grid.getCell(startingCell.getRow() + 1, startingCell.getColumn()),
            grid.getCell(startingCell.getRow(), startingCell.getColumn() - 1),
            grid.getCell(startingCell.getRow(), startingCell.getColumn() + 1)
        };
    }

}
