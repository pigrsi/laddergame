package com.pigrsi.dan.entityhelpers;

import com.badlogic.gdx.math.Interpolation;
import com.pigrsi.dan.entities.types.Entity;

public class RailMover extends BasicMover {
    public RailMover(Entity entity, float destinationX, float destinationY) {
        super(entity, destinationX, destinationY);
        setInterpolation(Interpolation.slowFast);
        setLifetime(0.075f);
    }
}
