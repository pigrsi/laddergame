package com.pigrsi.dan.entityhelpers;

import com.pigrsi.dan.entities.types.Entity;

import java.util.HashMap;
import java.util.Map;

public class ClimbingMap {
    private Map<Integer, Boolean> climbingMap = new HashMap<>();

    public void addClimbable(int ...id) {
        for (int i = 0; i < id.length; ++i) climbingMap.put(id[i], true);
    }

    public boolean canClimb(Entity entity) { return climbingMap.containsKey(entity.getCollisionID()); }
}
