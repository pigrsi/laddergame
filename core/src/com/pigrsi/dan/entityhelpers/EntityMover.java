package com.pigrsi.dan.entityhelpers;

import com.badlogic.gdx.math.Interpolation;

public interface EntityMover {
    void update(float delta);
    void setInterpolation(Interpolation interp);
    void setLifetime(float lifetime);
    boolean isDone();
}
