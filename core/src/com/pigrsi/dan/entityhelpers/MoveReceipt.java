package com.pigrsi.dan.entityhelpers;

import com.pigrsi.dan.entities.types.Entity;

import java.util.Set;

public class MoveReceipt {
    private Entity entity;
    private Entity.MoveType moveType;
    private int rowOffset;
    private int columnOffset;
    private Set<Entity> entitiesToBePushed;

    public MoveReceipt(Entity entity, Entity.MoveType moveType, int rowOffset, int columnOffset, Set<Entity> entitiesToBePushed) {
        this.entity = entity;
        this.moveType = moveType;
        this.rowOffset = rowOffset;
        this.columnOffset = columnOffset;
        this.entitiesToBePushed = entitiesToBePushed;
    }

    public Entity getEntity() {
        return entity;
    }

    public Entity.MoveType getMoveType() {
        return moveType;
    }

    public int getRowOffset() {
        return rowOffset;
    }

    public int getColumnOffset() {
        return columnOffset;
    }

    public Set<Entity> getEntitiesToBePushed() {
        return entitiesToBePushed;
    }
}
