package com.pigrsi.dan.animators;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pigrsi.dan.gameworld.World;
import com.pigrsi.dan.entities.types.Entity;

public class BasicAnimator implements Animator {

    Entity entity;
    protected Sprite sprite;
    protected float width;

    public BasicAnimator(Entity entity, String texture, int width, int height) {
        this.entity = entity;
        this.sprite = new Sprite(new Texture(texture), width, height);
        this.width = width;
    }

    public BasicAnimator(Entity entity, String texture) {
        this(entity, texture, World.CELL_SIZE, World.CELL_SIZE);
    }

    @Override
    public void render(SpriteBatch batch) {
        if (!entity.isAlive()) sprite.setColor(Color.BLACK);
        else sprite.setColor(Color.WHITE);
        sprite.setPosition(entity.getX() + (World.CELL_SIZE - width)/2, entity.getY());
        sprite.draw(batch);
    }

    @Override
    public void dispose() { sprite.getTexture().dispose(); }
}
