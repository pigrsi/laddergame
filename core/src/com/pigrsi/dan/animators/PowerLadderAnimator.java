package com.pigrsi.dan.animators;

import com.badlogic.gdx.graphics.Texture;
import com.pigrsi.dan.entities.ladder.Ladder;

public class PowerLadderAnimator extends LadderAnimator {
    public PowerLadderAnimator(Ladder ladder) {
        super(ladder, "ladder.png");
        sprite.setTexture(new Texture("rainbow_ladder.png"));
    }
}
