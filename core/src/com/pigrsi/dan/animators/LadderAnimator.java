package com.pigrsi.dan.animators;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pigrsi.dan.gameworld.World;
import com.pigrsi.dan.entities.ladder.Ladder;
import com.pigrsi.dan.entityhelpers.OrbitMover;

public class LadderAnimator implements RotaryAnimator {
    private Ladder ladder;
    private OrbitMover orbitMover;
    private float startRotation;
    Sprite sprite;

    public LadderAnimator(Ladder ladder, String texture) {
        this.ladder = ladder;
        this.sprite = new Sprite(new Texture(texture));
        sprite.setSize(World.CELL_SIZE, World.CELL_SIZE);
        startRotation = sprite.getRotation();
    }

    @Override
    public void render(SpriteBatch batch) {
        if (orbitMover != null) {
            float rotation = startRotation + orbitMover.getDegreesToOrbit() * orbitMover.getProgress();
            sprite.setRotation(rotation);
            sprite.setAlpha(0.5f);
            if (orbitMover.isDone()) orbitMover = null;
        } else {
            sprite.setAlpha(1f);
            switch (ladder.getOrientation()) {
                case UP: sprite.setRotation(0); break;
                case DOWN: sprite.setRotation(180); break;
                case LEFT: sprite.setRotation(90); break;
                case RIGHT: sprite.setRotation(-90); break;
            }
        }

        sprite.setPosition(ladder.getX(), ladder.getY());
        sprite.draw(batch);
    }

    @Override
    public void rotateWithOrbit(OrbitMover orbitMover) {
        if (this.orbitMover != null) {
            // Skip to end of previous rotation if new one is being set
            sprite.setRotation(startRotation + this.orbitMover.getDegreesToOrbit());
        }
        this.orbitMover = orbitMover;
        this.startRotation = sprite.getRotation();
    }

    @Override
    public void dispose() {
        sprite.getTexture().dispose();
    }
}
