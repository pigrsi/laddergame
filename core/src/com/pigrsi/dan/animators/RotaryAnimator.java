package com.pigrsi.dan.animators;

import com.pigrsi.dan.entityhelpers.OrbitMover;

public interface RotaryAnimator extends Animator {
    void rotateWithOrbit(OrbitMover orbitMover);
}
