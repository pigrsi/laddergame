package com.pigrsi.dan.animators;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;

public interface Animator extends Disposable {
    void render(SpriteBatch batch);
}
