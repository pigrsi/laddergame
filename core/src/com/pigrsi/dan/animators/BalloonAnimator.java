package com.pigrsi.dan.animators;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pigrsi.dan.entities.block.Balloon;
import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.gameworld.World;

public class BalloonAnimator extends BasicAnimator {
    public BalloonAnimator(Entity entity, String texture) {
        super(entity, texture, World.CELL_SIZE, World.CELL_SIZE);
    }

    @Override
    public void render(SpriteBatch batch) {
        if (sprite == null) return;
        if (!entity.isAlive()) sprite.setColor(Color.BLACK);
        else sprite.setColor(Color.WHITE);
        sprite.setPosition(entity.getX() + (World.CELL_SIZE - width)/2, entity.getY());
        sprite.draw(batch);
    }

    public void colourChanged() {
        Balloon balloon =(Balloon) entity;
        switch (balloon.colour) {
            case RED:
                sprite.setTexture(new Texture("red_balloon.png"));
                break;
            case BLUE:
                sprite.setTexture(new Texture("blue_balloon.png"));
                break;
            case GREEN:
                sprite.setTexture(new Texture("green_balloon.png"));
                break;
        }
    }
}
