package com.pigrsi.dan.animators;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pigrsi.dan.entities.types.Entity;
import com.pigrsi.dan.gameworld.World;

import static com.pigrsi.dan.gameworld.World.CELL_SIZE;

public class RailAnimator extends BasicAnimator {
    public RailAnimator(Entity entity) {
        super(entity, "rail.png", 80, CELL_SIZE);
    }

    @Override
    public void render(SpriteBatch batch) {
        sprite.setPosition(entity.getX() - 8, entity.getY());
        sprite.draw(batch);
    }
}