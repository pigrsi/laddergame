package com.pigrsi.dan.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

public class InputReceiver implements InputProcessor {

    private InputListener listener;

    public void addListener(InputListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean keyDown(int keycode) {
        switch(keycode) {
            case Input.Keys.UP:
                listener.inputUp();
                break;
            case Input.Keys.DOWN:
                listener.inputDown();
                break;
            case Input.Keys.LEFT:
                listener.inputLeft();
                break;
            case Input.Keys.RIGHT:
                listener.inputRight();
                break;
            case Input.Keys.BACKSPACE:
                listener.inputBackspace();
                break;
            case Input.Keys.ENTER:
                listener.inputEnter();
                break;
            case Input.Keys.SHIFT_RIGHT:
                listener.inputShiftRight();
                break;
            case Input.Keys.CONTROL_LEFT:
                listener.inputControlLeft();
                break;
            case Input.Keys.CONTROL_RIGHT:
                listener.inputControlRight();
                break;
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        int number = 400;
        int screenWidth = Gdx.graphics.getWidth();
        int screenHeight = Gdx.graphics.getHeight();
        if (screenX > screenWidth - number) listener.inputRight();
        else if (screenX < number) listener.inputLeft();
        else if (screenY > screenHeight - (number/2)) listener.inputDown();
        else if (screenY < (number/2)) listener.inputUp();
        else {
            listener.inputEnter();
        }
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
