package com.pigrsi.dan.input;

public interface InputListener {
    boolean inputUp();
    boolean inputDown();
    boolean inputLeft();
    boolean inputRight();
    boolean inputEnter();
    boolean inputBackspace();
    boolean inputShiftRight();
    boolean inputControlLeft();
    boolean inputControlRight();
}

